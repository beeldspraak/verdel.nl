CKEDITOR.on('instanceReady', function(ev) {
	var $script = document.createElement('script'),
	$editor_instance = CKEDITOR.instances[ev.editor.name];
	$script.src = '//use.typekit.net/uil5wei.js';
	$script.onload = function() {
		try{$editor_instance.window.$.Typekit.load();}catch(e){}
	};
	$editor_instance.document.getHead().$.appendChild($script);
	}
);
