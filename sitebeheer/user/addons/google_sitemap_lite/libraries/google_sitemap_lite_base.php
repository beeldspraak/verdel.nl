<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Google Sitemap Lite base file
 *
 * @package		Google Sitemap Lite
 * @category	Modules
 * @author		Rein de Vries <info@reinos.nl>
 * @link        http://reinos.nl/add-ons/google-sitemap-lite
 * @copyright 	Copyright (c) 2013 Reinos.nl Internet Media
 */

abstract class Google_sitemap_lite_base
{


    /**
     * Format the urls for only taxonomy
     *
     * @param $data
     * @return unknown_type
     */
    public function format_url($data) {

        //build the segments
        $template_group =   $data['group_name'];
        $template_name =    '/'.$data['template_name'];
        $url_title =        '/'.$data['url_title'];

        //when the index is set, add a forslash
        if(!empty($this->site_index)) {
            $template_group =   '/'.$template_group;
        }

        // don't display /index
        if($template_name == '/index')
        {
            $template_name = '';
        }

        //build the url
        $node_url =   $this->EE->functions->fetch_site_index().$template_group.$template_name.$url_title;

        // override template and entry slug with custom url if set
        if($data['custom_url'] != '')
        {
            $node_url = $data['custom_url'];
        }

        return $node_url;
    }

    //----------------------------------------------------------------------------

    /**
     * Build the sitemap
     *
     * @return mixed
     */
    abstract function build_sitemap();

    //----------------------------------------------------------------------------

    /**
     * Set the site url
     *
     * @return mixed
     */
    abstract function set_site_url();

    //----------------------------------------------------------------------------

    /**
     * Set an option array
     *
     * @param array $options
     * @return mixed
     */
    abstract function set_options($options = array());
}
