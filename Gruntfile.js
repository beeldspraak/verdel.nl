module.exports = function(grunt) {
	
	grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	
	grunt.initConfig({
		
		pkg: grunt.file.readJSON('package.json'),
		
		less: {
			development: {
				options: {
					compress: true,
					yuicompress: true,
					optimization: 2
				},
				files: { 
					'assets/dist/css/verdelictenmedia.min.css':	'assets/src/less/verdelictenmedia/theme.less',
					'assets/dist/css/wygwam.min.css': 			'assets/src/less/wygwam/wygwam.less', // destination file and source file
				}
			}
		},
		concat: {
			wygwam_js: {
				files: {
					'assets/dist/js/libs/wygwam/styles/verdel-wygwam-styles.min.js': [
						'assets/dist/js/verdel-wygwam-styles.js'
					]
				}
			},
			bootstrap_js: {
				src: [
					'assets/src/js/bootstrap/transition.js',
					'assets/src/js/bootstrap/alert.js',
					'assets/src/js/bootstrap/button.js',
					'assets/src/js/bootstrap/carousel.js',
					'assets/src/js/bootstrap/collapse.js',
					'assets/src/js/bootstrap/dropdown.js',
					'assets/src/js/bootstrap/modal.js',
					'assets/src/js/bootstrap/tooltip.js',
					'assets/src/js/bootstrap/popover.js',
					'assets/src/js/bootstrap/scrollspy.js',
					'assets/src/js/bootstrap/tab.js',
					'assets/src/js/bootstrap/affix.js'
				],
				dest: 'assets/src/js/bootstrap/bootstrap.js'
			}
		},
		copy: {
			fonts: {
				expand: true,
				cwd: 'assets/src',
				src: 'fonts/**',
				dest: 'assets/dist/'
			},
			jquery: {
				expand: true,
				cwd: 'assets/src',
				src: 'js/jquery/**',
				dest: 'assets/dist/'
			},
			wygwam: {
				expand: true,
				cwd: 'assets/src',
				src: 'js/wygwam/**',
				dest: 'assets/dist/'
			},
			css: {
				expand: true,
				cwd: 'assets/src',
				src: 'css/**',
				dest: 'assets/dist/'
			},
			img: {
				expand: true,
				cwd: 'assets/src',
				src: 'img/**',
				dest: 'assets/dist/'
			},
			ico: {
				expand: true,
				cwd: 'assets/src',
				src: 'favicon/**',
				dest: 'assets/dist/'
			}
		},
		watch: {
            src: {
                files: ['*.html'],
                options: { livereload: true }
            },
			styles: {
				files: ['assets/src/less/**/*.less'], // which files to watch
				tasks: ['less'],
				options: {
					spawn: false,
					livereload: true
				}
			}
		},
		uglify: {
			wygwam_js: {
				files: {
					'assets/dist/js/wygwam/styles/verdel-wygwam-styles.min.js': [
						'assets/src/js/wygwam/styles/verdel-wygwam-styles.js'
					]
				}
			},
			build: {
				files: {
					'assets/dist/js/verdelictenmedia.min.js': [
						'<%= concat.bootstrap_js.dest %>',
						'assets/src/js/fancybox/jquery.fancybox.js',
						'assets/src/js/smooth-scroll/jquery.smooth-scroll.js',
						'assets/src/js/validate/jquery.validate.js',
						'assets/src/js/validate/additional-methods.js',
						'assets/src/js/validate/localization/messages_nl.js',
						'assets/src/js/swiper/swiper.min.js',
						'assets/src/js/modernizr/modernizr.min.js',
						'assets/src/js/wow/wow.min.js',
						'assets/src/js/matchheight/jquery.matchHeight.js',
						'assets/src/js/init.js'
					]
				}
			}
		}
	});
			
	grunt.registerTask('default', ['less', 'concat', 'copy', 'watch', 'uglify']);
};
