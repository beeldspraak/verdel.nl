<div class="box panel">
    <div class="panel-heading">
        <h3 class="title-bar__title"><?=lang('external_sources')?></h3>
    </div>

    <div class="panel-body">
    <?php
        echo form_open($edit_source_action);
        $this->load->view('mcp/components/source_type_list', array('sources' => $sources));
    ?>
    </div>

    <div class="panel-footer">
        <div class="form-btns">
        <?php
            echo form_submit(array('name' => 'submit', 'value' => lang('add_new_source'), 'class' => 'btn'));
            echo form_close();

        if (!empty($sources)) {
            echo form_open($delete_source_action, 'id="delete_source"');
            echo '<input id="source_id" type="hidden" name="source_id" value="0" />';
            echo form_close();
        }
        ?>
        </div>
    </div>
</div>
