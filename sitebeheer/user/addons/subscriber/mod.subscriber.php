<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'subscriber/config.php';

/**
 * Addon Core Module File
 */
class Subscriber
{
	var $return_data = '';

	public function form()
	{
		$form_id = ee()->TMPL->fetch_param('form_id');

		$form = ee('Model')->get('subscriber:SubscriberForm')
			->filter('id', $form_id);

		if ($form->count() > 0)
		{
			ee()->load->helper('form');
			$this->return_data .= form_hidden('subscriber_form_id[]', $form_id);
		}

		if ($list_id = ee()->TMPL->fetch_param('list_id'))
		{
			$this->return_data .= form_hidden("subscriber_list_id[{$form_id}]", $list_id);
		}

		return $this->return_data;
	}
}

// EOF
