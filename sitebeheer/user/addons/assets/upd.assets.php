<?php

require_once 'addon.setup.php';
require_once PATH_THIRD . 'assets/libraries/LegacyUpdates.php';

use EEHarbor\Assets\FluxCapacitor\Base\Upd;
use EEHarbor\Assets\FluxCapacitor\Conduit\Version;
use EEHarbor\Assets\Libraries\LegacyUpdates;

class Assets_upd extends Upd
{
    public $has_cp_backend     = 'y';
    public $has_publish_fields = 'n';

    public function __construct($switch = true)
    {
        parent::__construct();

        require_once PATH_THIRD . 'assets/helper.php';
    }

    public function install()
    {
        ee()->load->dbforge();

        // -------------------------------------------
        //  Add row to exp_modules
        // -------------------------------------------

        ee()->db->insert('modules', array(
            'module_name'        => $this->flux->getConfig('name'),
            'module_version'     => $this->flux->getConfig('version'),
            'has_cp_backend'     => 'y',
            'has_publish_fields' => 'n'
        ));

        // -------------------------------------------
        //  Add rows to exp_actions
        // -------------------------------------------

        // file manager actions
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'upload_file'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_files_view_by_folders'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_props'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'save_props'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_ordered_files_view'));

        // Indexing actions
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_session_id'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'start_index'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'perform_index'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'finish_index'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_s3_buckets'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_gc_buckets'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_rs_regions'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_rs_containers'));

        // folder/file CRUD actions
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'move_folder'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'rename_folder'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'create_folder'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'delete_folder'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'view_file'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'move_file'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'delete_file'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'view_thumbnail'));

        // field actions
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'build_sheet'));
        ee()->db->insert('actions', array('class' => 'Assets_mcp', 'method' => 'get_selected_files'));

        // -------------------------------------------
        //  Create the exp_assets table
        // -------------------------------------------

        $fields = array(
            'file_id'           => array('type' => 'int', 'constraint' => 10, 'unsigned' => true, 'auto_increment' => true),
            'folder_id'         => array('type' => 'int', 'constraint' => 10, 'unsigned' => true, 'null' => false),
            'source_type'       => array('type' => 'varchar', 'constraint' => 2, 'null' => false, 'default' => 'ee'),
            'source_id'         => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'filedir_id'        => array('type' => 'int', 'constraint' => 4, 'unsigned' => true),
            'file_name'         => array('type' => 'varchar', 'constraint' => 255, 'null' => false),
            'title'             => array('type' => 'varchar', 'constraint' => 100),
            'date'              => array('type' => 'int', 'constraint' => 10, 'unsigned' => true, 'null' => true),
            'alt_text'          => array('type' => 'tinytext'),
            'caption'           => array('type' => 'tinytext'),
            'author'            => array('type' => 'tinytext'),
            '`desc`'            => array('type' => 'text'),
            'location'          => array('type' => 'tinytext'),
            'keywords'          => array('type' => 'text'),
            'date_modified'     => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'kind'              => array('type' => 'varchar', 'constraint' => 5),
            'width'             => array('type' => 'int', 'constraint' => 2),
            'height'            => array('type' => 'int', 'constraint' => 2),
            'size'              => array('type' => 'int', 'constraint' => 3),
            'search_keywords'   => array('type' => 'text')
        );

        ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('file_id', true);
        ee()->dbforge->create_table('assets_files');

        // ee()->db->query('ALTER TABLE exp_assets_files ADD UNIQUE unq_folder_id__file_name (folder_id, file_name)');

        // -------------------------------------------
        //  Create the exp_assets_selections table
        // -------------------------------------------

        $fields = array(
            'file_id'       => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'entry_id'      => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'field_id'      => array('type' => 'int', 'constraint' => 6, 'unsigned' => true),
            'col_id'        => array('type' => 'int', 'constraint' => 6, 'unsigned' => true),
            'row_id'        => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'var_id'        => array('type' => 'int', 'constraint' => 6, 'unsigned' => true),
            'element_id'    => array('type' => 'varchar', 'constraint' => 255, 'null' => true),
            'content_type'  => array('type' => 'varchar', 'constraint' => 255, 'null' => true),
            'sort_order'    => array('type' => 'int', 'constraint' => 4, 'unsigned' => true),
            'is_draft'      => array('type' => 'TINYINT', 'constraint' => '1', 'unsigned' => true, 'default' => 0)
        );

        ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('file_id');
        ee()->dbforge->add_key('entry_id');
        ee()->dbforge->add_key('field_id');
        ee()->dbforge->add_key('col_id');
        ee()->dbforge->add_key('row_id');
        ee()->dbforge->add_key('var_id');
        ee()->dbforge->create_table('assets_selections');

        // folder structure
        $fields = array(
            'folder_id'     => array('type' => 'int', 'constraint' => 10, 'unsigned' => true, 'auto_increment' => true),
            'source_type'   => array('type' => 'varchar', 'constraint' => 2, 'null' => false, 'default' => 'ee'),
            'folder_name'   => array('type' => 'varchar', 'constraint' => 255, 'null' => false),
            'full_path'     => array('type' => 'varchar', 'constraint' => 255),
            'parent_id'     => array('type' => 'int', 'constraint' => 10, 'unsigned' => true, 'null' => true),
            'source_id'     => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'filedir_id'    => array('type' => 'int', 'constraint' => 4, 'unsigned' => true),
        );

        ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('folder_id', true);
        ee()->dbforge->create_table('assets_folders');

        // ee()->db->query('ALTER TABLE exp_assets_folders ADD UNIQUE unq_source_type__source_id__filedir_id__parent_id__folder_name (`source_type`, `source_id`, `filedir_id`, `parent_id`, `folder_name`)');
        // ee()->db->query('ALTER TABLE exp_assets_folders ADD UNIQUE unq_source_type__source_id__filedir_id__full_path (`source_type`, `source_id`, `filedir_id`, `full_path`)');

        // source information
        $fields = array(
            'source_id'     => array('type' => 'int', 'constraint' => 10, 'unsigned' => true, 'auto_increment' => true),
            'source_type'   => array('type' => 'varchar', 'constraint' => 2, 'null' => false, 'default' => 's3'),
            'name'          => array('type' => 'varchar', 'constraint' => 255, 'null' => false, 'default' => ''),
            'settings'      => array('type' => 'text', 'null' => false)
        );

        ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('source_id', true);
        ee()->dbforge->create_table('assets_sources');

        // table for temporary data during indexing
        $fields = array(
            'session_id'    => array('type' => 'char', 'constraint' => 36),
            'source_type'   => array('type' => 'varchar', 'constraint' => 2, 'null' => false, 'default' => 'ee'),
            'source_id'     => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'offset'        => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'uri'           => array('type' => 'varchar', 'constraint' => 255),
            'filesize'      => array('type' => 'int', 'constraint' => 10, 'unsigned' => true),
            'type'          => array('type' => 'enum', 'constraint' => "'file','folder'"),
            'record_id'     => array('type' => 'int', 'constraint' => 10, 'unsigned' => true)
        );
        ee()->dbforge->add_field($fields);
        ee()->dbforge->create_table('assets_index_data');
        ee()->db->query('ALTER TABLE `exp_assets_index_data` ADD UNIQUE unq__session_id__source_type__source_id__offset (`session_id`, `source_type`, `source_id`, `offset`)');

        $fields = array(
            'connection_key' => array('type' => 'varchar', 'constraint' => 191, 'null' => false, 'required' => true),
            'token'          => array('type' => 'varchar', 'constraint' => 255, 'null' => false, 'required' => true),
            'storage_url'    => array('type' => 'varchar', 'constraint' => 255, 'null' => false, 'required' => true),
            'cdn_url'        => array('type' => 'varchar', 'constraint' => 255, 'null' => false, 'required' => true)
        );

        ee()->dbforge->add_field($fields);
        ee()->dbforge->add_key('connection_key', true);
        ee()->dbforge->create_table('assets_rackspace_access');

        return true;
    }

    /**
     * Update
     */
    public function update($current = '')
    {
        if ($current == $this->version) {
            return false;
        }

        // All updates from 2.5 and earlier are in the Legacy Updates class.
        // This was moved for sanity
        if (version_compare($current, '2.5', '<')) {
            $legacyUpdates = new LegacyUpdates();
            $legacyUpdates->update($current);
        }

        // If older version
        if (version_compare($current, '3.2.0', '<')) {
            $version = new Version();
            $settings = Assets_helper::get_global_settings();

            if (!empty($settings['license_key'])) {
                $data = array(
                    'site_id' => ee()->config->item('site_id'),
                    'addon' => 'EEHarbor\Assets',
                    'license_key' => $settings['license_key'],
                );

                $version->saveLicenseKey($data);
            }
        }

        if (version_compare($current, '4.0.0', '<')) {
            $results = ee()->db->select('source_id, settings')
                ->from('exp_assets_sources')
                ->get();

            if (!empty($results->result_array())) {
                foreach ($results->result_array() as $row) {
                    $settings = json_decode($row['settings']);
                    $current_location = $settings->location;

                    if ($current_location === "US") {
                        $settings->location = "us-east-1";
                    }
                    if ($current_location === "EU") {
                        $settings->location = "eu-west-1";
                    }

                    $updated = json_encode($settings);
                    ee()->db->update(
                        'exp_assets_sources',
                        array(
                            'settings'  => $updated,
                        ),
                        array(
                            'source_id' => $row['source_id']
                        )
                    );
                }
            }
        }

        // The parent update class also makes sure the extensions are properly installed after updating
        parent::update($current);

        // -------------------------------------------
        //  Update version number in exp_fieldtypes and exp_extensions
        // -------------------------------------------

        ee()->db->where('name', 'assets')
            ->update('fieldtypes', array('version' => $this->flux->getConfig('version')));

        ee()->db->where('class', 'Assets_ext')
            ->update('extensions', array('version' => $this->flux->getConfig('version')));

        return true;
    }

    /**
     * Uninstall
     */
    public function uninstall()
    {
        ee()->load->dbforge();

        // routine EE table cleanup

        ee()->db->select('module_id');
        $module_id = ee()->db->get_where('modules', array('module_name' => 'Assets'))->row('module_id');

        $module_member_groups_table = version_compare(APP_VER, '6.0', '>=') ? 'module_member_roles' : 'module_member_groups';
        ee()->db->where('module_id', $module_id);
        ee()->db->delete($module_member_groups_table);

        ee()->db->where('module_name', 'Assets');
        ee()->db->delete('modules');

        ee()->db->where('class', 'Assets');
        ee()->db->delete('actions');

        ee()->db->where('class', 'Assets_mcp');
        ee()->db->delete('actions');

        // drop Assets tables
        ee()->dbforge->drop_table('assets_files');
        ee()->dbforge->drop_table('assets_selections');
        ee()->dbforge->drop_table('assets_sources');
        ee()->dbforge->drop_table('assets_folders');
        ee()->dbforge->drop_table('assets_index_data');
        ee()->dbforge->drop_table('assets_rackspace_access');

        $cache_path = ee()->config->item('cache_path');
        if (empty($cache_path)) {
            $cache_path = APPPATH . 'cache/';
        }

        $this->_delete_recursively($cache_path . 'assets');
        return true;
    }

    /**
     * Delete folder recursively
     * @param $folder
     */
    private function _delete_recursively($folder)
    {
        foreach (glob($folder . '/*') as $file) {
            if (is_dir($file)) {
                $this->_delete_recursively($file);
            } else {
                @unlink($file);
            }
        }
        @rmdir($folder);
    }
}
