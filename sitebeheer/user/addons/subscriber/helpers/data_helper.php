<?php

/**
 * Returns the value of an input using a data array first, then post value
 * and then a default value
 *
 * @access private
 * @param Model $model The SubscriberForm model
 * @param String $input_name The name of the input field separated by dots
 * 	for nested fields
 * @param String $default_value The value to default to
 * @return String The correct value for the input field
 */
function getValue($model, $input_name, $default_value = '')
{
	if (($value = getArrayValue($model, $input_name)) !== NULL)
	{
		return $value;
	}
	else if (($value = getArrayValue($_POST, $input_name)) !== NULL)
	{
		return $value;
	}
	else
	{
		return $default_value;
	}
}

/**
 * Get a nested array value given a dot-separated path
 *
 * @param  array  $array Array to traverse
 * @param  string $path  Dot-separated path
 * @return mixed         Array value
 */
function getArrayValue($array, $path)
{
	$path = explode('.', $path);

	if (is_array($array))
	{
		for ($i = $array; $key = array_shift($path); $i = $i[$key])
		{
			if ( ! isset($i[$key]))
			{
				return NULL;
			}
		}
	}
	else if (is_object($array))
	{
		$i = $array->{$path[0]};

		if (is_array($i) && count($path) > 1)
		{
			array_shift($path);
			for ($i = $i; $key = array_shift($path); $i = $i[$key])
			{
				if ( ! isset($i[$key]))
				{
					return NULL;
				}
			}
		}
	}

	return $i;
}

// EOF
