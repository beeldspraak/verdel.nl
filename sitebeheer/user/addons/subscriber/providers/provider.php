<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Provider Abstract Class
 *
 * @since 4.0
 */
abstract class Provider
{
	/**
	 * Name of the Provider, used in logs and settings page
	 * @var string
	 */
	protected $name = '';

	/**
	 * Prefix for the particular provider
	 * @var string
	 */
	protected $prefix = '';

	/**
	 * Fields used by provdier, should have defaults
	 * @var array
	 */
	protected $fields = array();

	/**
	 * Values of the form
	 * @var array
	 */
	protected $values = array();

	/**
	 * Custom fields as the keys and type (input vs checkbox) for the value
	 * @var array
	 */
	protected $custom_fields = array();

	public function __construct()
	{
		ee()->load->helper('data');
	}

	/**
	 * Sends the new subscriber to the provider
	 *
	 * @param  Array $data Form data provided by Freeform
	 * @param  Model $model The SubscriberForm model
	 * @return Void
	 */
	abstract public function subscribe($data, $model);

	/**
	 * Unsubscribe a user from the provider
	 *
	 * @param  Array $data Form data provided by Freeform
	 * @param  Model $model The SubscriberForm model
	 * @return void
	 */
	abstract public function unsubscribe($data, $model);

	/**
	 * Returns an associative array containing data about the API key and list
	 * ID form fields for the shared form service.
	 *
	 * @param Model $model The SubscriberForm model
	 * @return Array Associative array
	 */
	abstract public function provider_fields($model);

	/**
	 * Returns an associative array containing data about the name fields for
	 * the shared form service.
	 *
	 * @param Model $model The SubscriberForm model
	 * @return Array Associative array
	 */
	abstract public function method_fields($model);

	/**
	 * Returns all of the available fields for a provider
	 *
	 * @param Model $model The SubscriberForm model
	 * @return Array Associative array containing all of the fields for a
	 *               provder
	 */
	public function fields($model)
	{
		return array(
			'provider' => $this->provider_fields($model),
			'method'   => $this->method_fields($model)
		);
	}

	/**
	 * Returns an associative array containing data about the custom fields
	 *
	 * @param Array $settings Settings for the form
	 * @return Array Associative array
	 */
	public function custom_fields($settings)
	{
		$grid = $this->createGrid($settings);
		$name = $this->prefix.'_custom_fields';
		return array(
			'desc' => $name,
			'group' => strtolower(str_replace(' ', '_', $this->name)),
			'grid' => TRUE,
			'wide' => TRUE,
			'fields' => array(
				"settings[{$name}]" => array(
					'type' => 'html',
					'content' => ee()->load->view('_shared/table', $grid->viewData(), TRUE)
				)
			)
		);
	}

	/**
	 * Create the Grid field for the custom fields
	 *
	 * @param Model $model The model data
	 * @return CP/GridInput The Grid object
	 */
	protected function createGrid($settings)
	{
		$name = $this->prefix.'_custom_fields';
		$grid = ee('CP/GridInput', array(
			'field_name' => "settings[{$name}]",
			'reorder'    => FALSE,
		));
		$grid->loadAssets();
		$grid->setColumns($this->custom_fields);
		$grid->setNoResultsText('no_custom_fields', 'add_custom_field');
		$grid->setBlankRow($this->getGridRow());

		if ( ! empty($settings[$name]))
		{
			$rows = array();

			foreach($settings[$name]['rows'] as $row_id => $custom_field)
			{
				$rows[] = array(
					'attrs' => array(
						'row_id' => str_replace(
							array('row_id_', 'new_row_'),
							'',
							$row_id
						)
					),
					'columns' => $this->getGridRow($custom_field),
				);
			}

			$grid->setData($rows);
		}

		return $grid;
	}

	/**
	 * Get a grid row, either empty or with data
	 *
	 * @param array $data The data to pass to the grid row
	 * @return array Array representation of grid row
	 */
	protected function getGridRow($data = array())
	{
		$defaults = array();

		foreach ($this->custom_fields as $name => $type)
		{
			$defaults[$name] = '';
		}

		$data = array_merge($defaults, $data);

		$columns = array();

		foreach ($this->custom_fields as $name => $type)
		{
			if ($type == 'input')
			{
				$html = form_input($name, $data[$name]);
			}
			else if ($type == 'checkbox')
			{
				$html = form_checkbox($name, 'y', ($data[$name] == 'y'));
			}

			$columns[]['html'] = $html;
		}

		return $columns;
	}

	/**
	 * Gets the name of the subscriber
	 * @param  Array $data Array of form data
	 * @param  Model $model The SubscriberForms model
	 * @param  string $key The name of the key in the model for the name field
	 * @param  string $default The default value to provide
	 * @return string The name to use
	 */
	protected function getName($data, $model, $key, $default = '')
	{
		if (isset($model->settings[$key]) && isset($data[$model->settings[$key]]))
		{
			return $data[$model->settings[$key]];
		}

		return $default;
	}

	/**
	 * Get the list ID, checking POST for overrides
	 * @param  Array $data Array of form data
	 * @param  Model $model The SubscriberForms model
	 * @return String The list ID to use
	 */
	protected function getListId($data, $model)
	{
		if (isset($data['subscriber_list_id'][$model->id]))
		{
			return $data['subscriber_list_id'][$model->id];
		}

		return $model->settings["{$this->prefix}_list_id"];
	}

	/**
	 * Logs API failures
	 * @param  string  $error_code    Error code from provider
	 * @param  string  $error_message Error message from provider
	 * @return Void
	 */
	protected function _log($error_code, $error_message)
	{
		ee()->load->library('logger');
		ee()->logger->developer("{$this->name} API Error: {$error_code}: {$error_message}");
	}

	/**
	 * __get magic method for returning class properties
	 * @param  string $name name of the property
	 * @return mixed        value of the property
	 */
	public function __get($name)
	{
		if ($name == 'short_name')
		{
			return strtolower(str_replace(' ', '_', $this->name));
		}
		else if (isset($this->values->{$name}))
		{
			return $this->values->{$name};
		}
		else if (isset($this->fields[$name]))
		{
			return $this->fields[$name];
		}

		return NULL;
	}
}

// EOF
