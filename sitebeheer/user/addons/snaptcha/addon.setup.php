<?php

return array(
    'author'      => 'PutYourLightsOn',
    'author_url'  => 'https://putyourlightson.com/',
    'name'        => 'Snaptcha',
    'description' => 'Invisible CAPTCHA to prevent spam form submissions.',
    'version'     => '3.1.1',
    'namespace'   => '\\',
    'settings_exist' => TRUE,
    'docs_url'    => 'https://putyourlightson.com/expressionengine-addons/snaptcha',
);
