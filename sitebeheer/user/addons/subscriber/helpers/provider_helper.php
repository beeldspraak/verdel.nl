<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(PATH_THIRD.'subscriber/config.php');


/**
 * Returns a list of all providers, with their short name as the key and an
 * instantiated object as the value
 * @return Array Array of providers
 */
function providers()
{
	$providers	= array();
	$noise		= array('.', '..', 'provider.php');
	$file_list	= array_diff(scandir(SUBSCRIBER_PROVIDERS), $noise);

	foreach ($file_list as $index => $file_name)
	{
		if ( ! strpos($file_name, '.') == 0)
		{
			$provider = load_provider($file_name);
			$providers[$provider->short_name] = $provider;
		}
	}

	return $providers;
}

/**
 * Load a provider given a file name or provider name
 * @param  String $provider_name provider name or file name of provider
 * @return Object                Instantiated provider
 */
function load_provider($provider_name)
{
	$pathinfo = pathinfo($provider_name);

	if ( empty($pathinfo['extension']) OR $pathinfo['extension'] !== 'php')
	{
		$file_name = 'provider.'.$provider_name.'.php';
		$class_name = 'Provider_'.$provider_name;
	}
	else
	{
		$file_name = $provider_name;
		$class_name = ucfirst(str_replace('.', '_', $pathinfo['filename']));
	}

	require_once(SUBSCRIBER_PROVIDERS.$file_name);
	return new $class_name;
}

/**
 * Given the provider list, and optionally settings, will return a list of the
 * providers
 * @return Array List of provider names
 */
function provider_list()
{
	$provider_names = array();

	foreach (providers() as $name => $provider)
	{
		$provider_names[$provider->short_name] = $provider->short_name;
	}

	return $provider_names;
}

/**
 * Given the provider list and a set of values, will return all standard fields
 * with their values populated
 * @param Model $model The SubscriberForm model
 * @return Array             Array of fields with form values
 */
function provider_fields($model)
{
	$merge = array();

	foreach (providers() as $name => $provider)
	{
		$fields = $provider->fields($model);

		// Merge together provider and method fields
		$merge = array_merge_recursive($merge, $fields);

		// Keep custom fields separate
		$merge['custom'][] = $provider->custom_fields($model->settings);
	}

	return $merge;
}

// EOF
