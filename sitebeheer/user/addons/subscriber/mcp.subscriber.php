<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'subscriber/config.php';
use ExpressionEngine\Library\CP\Table;

/**
* Addon Control Panel
*/
class Subscriber_mcp
{
	function __construct()
	{
		// Setup the base url to the module
		$this->form_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=subscriber';
		$this->base = BASE.AMP.$this->form_base;

		$this->base = ee('CP/URL', 'addons/settings/subscriber');
		$this->subscriber_edit = ee('CP/URL', 'addons/settings/subscriber/view');

		ee()->load->helper(array('provider', 'data'));
	}

	// ------------------------------------------------------------------------

	public function index()
	{
		$table = ee('CP/Table', array(
			'autosort'   => TRUE,
			'autosearch' => FALSE,
			'class'      => 'settings',
			'sort_col'   => 'token'
		));

		$table->setColumns(array(
			'form_name',
			'token' => array(
				'encode' => FALSE
			),
			'manage' => array(
				'type' => Table::COL_TOOLBAR
			),
			array(
				'type' => Table::COL_CHECKBOX
			)
		));
		$table->setNoResultsText(
			'no_forms',
			'new_form',
			$this->subscriber_edit
		);

		$data = array();
		foreach (ee('Model')->get('subscriber:SubscriberForm')->all() as $form)
		{
			$data[] = array(
				array(
					'content' => $form->form_name,
					'href'    => ee('CP/URL')->make('addons/settings/subscriber/view', array('form_id' => $form->id))
				),
				array(
					'content' => '<code>{exp:subscriber:form form_id="' . $form->id . '"}</code>',
					'class'   => 'code'
				),
				array('toolbar_items' => array(
					'copy' => array(
						'href'  => ee('CP/URL')->make('addons/settings/subscriber/duplicate', array('form_id' => $form->id)),
						'title' => lang('duplicate')
					)
				)),
				array(
					'name'  => 'forms[]',
					'value' => $form->id,
					'data' => array(
						'confirm' => lang('form_name') . ': <b>' . htmlentities($form->form_name, ENT_QUOTES, 'UTF-8') . '</b>'
					)
				)
			);
		}
		$table->setData($data);

		$vars = array(
			'table'    => $table->viewData($this->base),
			'new_form' => $this->subscriber_edit,
			'base_url' => $this->base
		);

		ee()->javascript->set_global('lang.remove_confirm', lang('form_names') . ': <b>### ' . lang('form_names') . '</b>');
		ee()->cp->add_js_script(array(
			'file' => array('cp/confirm_remove'),
		));

		return array(
			'body'    => ee('View')->make('subscriber:index')->render($vars),
			'heading' => lang('subscriber_module_name')
		);
	}

	// ------------------------------------------------------------------------

	public function view()
	{
		$vars = array(
			'base_url'              => ee('CP/URL', 'addons/settings/subscriber/view'),
			'cp_page_title'         => lang('new_form'),
			'save_btn_text'         => 'btn_save_settings',
			'save_btn_text_working' => 'btn_saving'
		);

		$data = array(
			'provider' => 'campaign_monitor',
			'settings' => array()
		);

		// Validate the form
		if ( ! empty($_POST))
		{
			$validator = ee('Validation')->make();

			$validator->defineRule('whenProviderIs', function($key, $value, $parameters, $rule) {
				return ($_POST['provider'] == $parameters[0]) ? TRUE : $rule->skip();
			});

			$rules = array(
				'form_name'   => 'required',
				'email_field' => 'required',
			);

			foreach (providers() as $name => $provider)
			{
				foreach ($provider->provider_fields($data) as $field)
				{
					foreach ($field['fields'] as $field_name => $field_settings)
					{
						if (isset($field_settings['required']))
						{
							$rules[$field_name] = "whenProviderIs[{$provider->prefix}]|required";
						}
					}
				}
			}

			$validator->setRules($rules);

			// Flatten settings for validation
			$data = $_POST;
			foreach ($_POST['settings'] as $key => $value)
			{
				$_POST["settings[{$key}]"] = $value;
			}

			$result = $validator->validate($_POST);

			if ($result->isValid())
			{
				$this->save();
			}
			else
			{
				$vars['errors'] = $result;
				ee('CP/Alert')->makeInline('shared-form')
					->asIssue()
					->withTitle(lang('form_not_created'))
					->now();
			}
		}

		if ($form_id = ee()->input->get('form_id'))
		{
			$form = ee('Model')->get('subscriber:SubscriberForm')
				->filter('id', $form_id)
				->first();

			$vars['form_hidden']['form_id'] = $form_id;
			$vars['cp_page_title']          = $form->form_name;
		}
		else
		{
			$form = ee('Model')->make('subscriber:SubscriberForm');
		}

		// Setup standard fields
		$vars['sections'] = array(
			array(
				array( // section
					'title' => 'form_name',
					'fields' => array(
						'form_name' => array(
							'type' => 'text',
							'value' => getValue($form, 'form_name'),
							'required' => TRUE
						)
					)
				),
			),
			'provider' => array(
				array(
					'title' => 'newsletter_provider',
					'fields' => array(
						'provider' => array(
							'type' => 'radio',
							'choices' => provider_list(),
							'value' => getValue($form, 'provider', 'cm'),
							'group_toggle' => array(
								'campaign_monitor' => 'campaign_monitor',
								'mailchimp' => 'mailchimp'
							),
						),
					)
				)
				// provider_fields[provider] imported here
			),
			'method' => array(
				// This field will control whether or not switch field is shown
				array(
					'title' => 'add_method',
					'desc' => 'method_desc',
					'fields' => array(
						'method' => array(
							'type' => 'radio',
							'choices' => array(
								'everyone' => 'everyone',
								'switch'   => 'switch_field'
							),
							'value' => getValue($form, 'method', 'everyone'),
							'group_toggle' => array(
								'everyone' => 'everyone',
								'switch' => 'switch'
							)
						)
					)
				),
				// provider_fields[method] imported here
				array(
					'title' => 'email_field',
					'desc' => 'email_note',
					'fields' => array(
						'email_field' => array(
							'type' => 'text',
							'required' => TRUE,
							'value' => getValue($form, 'email_field', 'email')
						)
					)
				),
				array(
					'title' => 'switch_input_field',
					'desc' => 'switch_input_field_desc',
					'group' => 'switch',
					'fields' => array(
						'switch_field' => array(
							'type' => 'text',
							'value' => getValue($form, 'switch_field', 'switch')
						)
					)
				),
				array(
					'title' => 'switch_input_field_value',
					'group' => 'switch',
					'fields' => array(
						'switch_value' => array(
							'type' => 'text',
							'value' => getValue($form, 'switch_value', 'yes')
						)
					)
				),
				array(
					'title' => 'unsubscribe',
					'desc' => 'unsubscribe_desc',
					'group' => 'switch',
					'fields' => array(
						'unsubscribe' => array(
							'type' => 'yes_no',
							'value' => getValue($form, 'unsubscribe', 'n')
						)
					)
				),
			),
		);

		$provider_fields = provider_fields($form);

		// Add provider fields
		$vars['sections']['provider'] = array_merge(
			$vars['sections']['provider'],
			$provider_fields['provider']
		);

		// Add method fields
		array_splice($vars['sections']['method'], 1, 0, $provider_fields['method']);

		// Add custom fields
		$vars['sections']['custom_fields'] = $provider_fields['custom'];

		ee()->cp->add_js_script(array('file' => array('cp/form_group')));

		return array(
			'body' => ee('View')->make('ee:_shared/form_with_box')->render($vars),
			'breadcrumb' => array(
				ee('CP/URL', 'addons/settings/subscriber')->compile() => lang('subscriber_forms')
			),
			'heading' => lang('subscriber_form')
		);
	}

	// -----------------------------------------------------------------------------

	private function save()
	{
		if ($form_id = ee()->input->post('form_id'))
		{
			$form = ee('Model')->get('subscriber:SubscriberForm')
				->filter('id', $form_id)
				->first();
		}
		else
		{
			$form = ee('Model')->make('subscriber:SubscriberForm');
		}

		$form->form_name    = ee()->input->post('form_name', TRUE);
		$form->provider     = ee()->input->post('provider', TRUE);
		$form->method       = ee()->input->post('method', TRUE);
		$form->email_field  = ee()->input->post('email_field');
		$form->switch_field = ee()->input->post('switch_field');
		$form->switch_value = ee()->input->post('switch_value');
		$form->unsubscribe  = ee()->input->post('unsubscribe');
		$form->settings     = ee()->input->post('settings');
		$form->save();

		ee()->session->set_flashdata(array(
			'message_success' => lang('preferences_updated')
		));

		ee()->functions->redirect($this->base);
	}

	// -----------------------------------------------------------------------------

	public function duplicate()
	{
		if ($form_id = ee()->input->get('form_id'))
		{
			$form = $form = ee('Model')->get('subscriber:SubscriberForm')
				->filter('id', $form_id)
				->first();

			// Actually clone it
			$form = ee('Model')->make($form->getName(), $form->toArray())->save();

			ee()->functions->redirect(ee('CP/URL')->make(
				'addons/settings/subscriber/view',
				array('form_id' => $form->id)
			));
		}
	}

	// -----------------------------------------------------------------------------

	public function delete()
	{
		foreach (ee()->input->post('forms') as $form_id)
		{
			ee('Model')->get('subscriber:SubscriberForm')
				->filter('id', $form_id)
				->first()
				->delete();
		}

		ee()->functions->redirect($this->base);
	}
}

// EOF
