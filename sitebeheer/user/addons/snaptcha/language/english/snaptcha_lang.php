<?php

$lang = array(
    'license_number' => 'License Number',
    'field_name' => 'Field Name Prefix (at least 4 characters)',
    'security_level' => 'Security Level',
    'low' => 'Low: javascript not required',
    'medium' => 'Medium: javascript required',
    'high' => 'High: javascript required, single form submission enforced',
    'error_message' => 'Error Message (HTML allowed)',
    'default_error_message' => 'Sorry, you have failed the security test. Please ensure that you have javascript enabled and that you refresh the page that you are trying to submit.',
    'member_registration_validation' => 'Member Registration Validation<br>(includes User Module and FreeMember)',
    'logging' => 'Rejected Form Submission Logging',
    'enabled' => 'Enabled',
    'disabled' => 'Disabled',
    'invalid_license' => 'Invalid License Number',
    'field_name_prefix_too_short' => 'The field name prefix must be at least four characters',
    'member_register_notice' => 'You must include the following code in the member registration form (inside the &lt;form&gt; tags)',
    'log_file_not_writable' => 'The log file is not writable. Please change file permissions to 666 to enable logging.',
    'save_settings' => 'Save Settings',
);
