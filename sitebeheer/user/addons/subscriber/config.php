<?php

if ( ! defined('SUBSCRIBER_NAME'))
{
	define('SUBSCRIBER_NAME',       'Subscriber');
	define('SUBSCRIBER_MACHINE',    'Subscriber');
	define('SUBSCRIBER_VER',        '6.0.0');
	define('SUBSCRIBER_DESC',       'Freeform Extension that sends Names, Email Addresses and custom fields to email newsletter providers.');
	define('SUBSCRIBER_DB_FORMS',   'subscriber_forms');
	define('SUBSCRIBER_PROVIDERS',  PATH_THIRD.'subscriber/providers/');
}
