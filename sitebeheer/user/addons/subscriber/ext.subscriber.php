<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'subscriber/config.php';

class Subscriber_ext
{
	public $name           = SUBSCRIBER_NAME;
	public $class_name     = 'Subscriber';
	public $version        = SUBSCRIBER_VER;
	public $description    = SUBSCRIBER_DESC;
	public $settings_exist = 'n';
	public $docs_url       = 'http://devot-ee.com/add-ons/subscriber/';

	private $settings       = array();

	public function __construct($settings = '')
	{
		$this->settings = $settings;
	}

	// Extension Functions =========================================================

	/**
	 * Activate Extension
	 * @access public
	 */
	public function activate_extension()
	{
		return TRUE;
	}

	/**
	 * Disable Extension
	 * @access public
	 */
	public function disable_extension()
	{
		ee()->db->delete('extensions', array("class" => ucfirst(get_class($this))));
	}

	/**
	 * Update Extension
	 * @access public
	 */
	public function update_extension($current = '')
	{
		return FALSE;
	}

	// Hooks ===================================================================

	/**
	 * Receives calls when email is sent using the email module
	 *
	 * @param string $subject Sanitized and parsed subject of the email
	 * @param string $message Sanitized and parsed body of the email
	 * @param array $approved_tos Email addresses in the form’s “to” field
	 * @param array $approved_recipients Email addresses specified in the tag as recipients
	 * @return void
	 */
	public function insert_subscriber_from_email($subject, $message, $approved_tos, $approved_recipients)
	{
		$post_data = $this->_gather_post_data();

		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, $post_data);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Insert a subscriber when added from a Channel Form
	 *
	 * @param object $channel_form_obj Active channel form object
	 * @return void
	 */
	public function insert_subscriber_from_channel_form($channel_form_obj)
	{
		$post_data = $this->_gather_post_data();

		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, $post_data);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Sends name and email address (and optionally custom fields) to Campaign Monitor
	 *
	 * @param Array $data Array of fields/values to be inserted for the Freeform form submission
	 * @return Array The same data passed into the function, unchanged
	 */
	public function insert_subscriber_from_freeform($data)
	{
		$subscriber_form_ids = $this->_get_subscriber_form_ids();

		foreach ($subscriber_form_ids as $form_id)
		{
			$this->_add_subscriber($form_id, $data);
		}

		// Clean up the subscriber_form_ids
		$data['subscriber_form_id'] = implode('|', $subscriber_form_ids);

		// Always return data
		return $data;
	}

	// -------------------------------------------------------------------------

	/**
	 * Create a subscriber from a Freeform Next submission
	 * @param  FormModel $model Freeform Next SubmissionForm model
	 * @param  boolean $isNew Whether the form is a new submission or not
	 * @return void
	 */
	public function insert_subscriber_from_freeform_next($model, $isNew)
	{
		$post_data = $this->_gather_post_data();

		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, $post_data);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Creates a subscriber from Solspace User registration
	 *
	 * @param  User $User User object
	 * @param  array $custom_fields Array of custom fields
	 * @param  integer $member_id Member ID
	 * @return void
	 */
	public function insert_subscriber_from_user($User, $custom_fields, $member_id)
	{
		$post_data = $this->_gather_post_data();

		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, $post_data);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Creates a subscriber from comment form data
	 *
	 * @param  array $data             Data for the new comment
	 * @param  bool  $comment_moderate TRUE if comment is being moderated
	 * @param  int   $comment_id       ID of comment
	 * @return void
	 */
	public function insert_subscriber_from_comment($data, $comment_moderate, $comment_id)
	{
		$post_data = $this->_gather_post_data();

		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, $post_data);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Create a subscriber from member registration data
	 * @param  array $data      Data for new member
	 * @param  int   $member_id Member ID
	 * @return void
	 */
	public function insert_subscriber_from_member_registration($data, $member_id)
	{
		$post_data = $this->_gather_post_data();

		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, $post_data);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Create a subscriber from CartThrob member registration or purchase
	 * @param  array $data      Data for new member
	 * @param  int   $member_id Member ID
	 * @return void
	 */
	public function insert_subscriber_from_cartthrob()
	{
		// Merge in the customer info from CartThrob
		try {
			$customer_info = ee()->cartthrob->cart->customer_info();
		} catch (Exception $e) {
			$customer_info = array();
		}

		$post_data = $this->_gather_post_data();

		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, array_merge($customer_info, $post_data));
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Create a subscriber from FormGrab submission
	 * @param  Object $sub      Form Submission model object
	 * @param  array $post      Sanitized POST data
	 * @return Object $sub, altered or not
	 */
	public function insert_subscriber_from_formgrab($sub, $post)
	{
		foreach ($this->_get_subscriber_form_ids() as $form_id)
		{
			$this->_add_subscriber($form_id, $post);
		}

		return $sub;
	}

	// Supporting Methods ======================================================

	/**
	 * Given a form_id and the data sent from Freeform, will get the settings
	 * from a form and send it to the right provider
	 *
	 * @param integer $form_id The id of the subscriber form we're using
	 * @param array $settings Associative array of form data
	 */
	private function _add_subscriber($form_id, $data)
	{
		ee()->load->helper('email');

		$form = ee('Model')->get('subscriber:SubscriberForm')
			->filter('id', $form_id)
			->first();

		$switch_valid = (ee()->input->get_post($form->switch_field) == $form->switch_value);

		if (($form->method == 'everyone' || $switch_valid)
			&& valid_email($data[$form->email_field]))
		{
			require_once(PATH_THIRD.'/subscriber/providers/provider.'.$form->provider.'.php');
			$class = 'Provider_'.$form->provider;
			$provider = new $class;
			$provider->subscribe($data, $form);
		}
		else if ($form->method == 'switch'
			&& ! $switch_valid
			&& $form->unsubscribe == 'y'
			&& valid_email($data[$form->email_field]))
		{
			require_once(PATH_THIRD.'/subscriber/providers/provider.'.$form->provider.'.php');
			$class = 'Provider_'.$form->provider;
			$provider = new $class;
			$provider->unsubscribe($data, $form);
		}
	}

	// -------------------------------------------------------------------------

	/**
	 * Returns all POST data, fully XSS cleaned
	 * @return array Associative array containing XSS cleaned POST data
	 */
	private function _gather_post_data()
	{
		$post_data = array();

		foreach ($_POST as $index => $raw_data)
		{
			// All POST data must be cleaned
			$post_data[$index] = ee()->input->post($index, TRUE);
		}

		return $post_data;
	}

	// -------------------------------------------------------------------------

	/**
	 * Get all subscriber form IDs associated with this form
	 * @return array Indexed array of subscriber form IDs
	 */
	private function _get_subscriber_form_ids()
	{
		$subscriber_form_id_raw = ee()->input->post('subscriber_form_id');
		$subscriber_form_ids = array();

		if (isset($subscriber_form_id_raw))
		{
			// Get the subscriber form ids and break em up
			if (is_array($subscriber_form_id_raw))
			{
				foreach ($subscriber_form_id_raw as $index => $value)
				{
					$subscriber_form_ids = array_merge($subscriber_form_ids, explode('|', $value));
				}
			}
			else
			{
				$subscriber_form_ids = explode('|', $subscriber_form_id_raw);
			}

			// Make sure IDs are unique
			$subscriber_form_ids = array_unique($subscriber_form_ids, SORT_NUMERIC);

			foreach ($subscriber_form_ids as $index => $form_id)
			{
				// Trim the form_id since there might be a lot of extra
				// whitespaces and newlines, also make it an integer
				$form_id = $subscriber_form_ids[$index] = (int) trim($form_id);

				// Remove 0's since there are no forms with 0 as an ID
				if ($form_id === 0)
				{
					unset($subscriber_form_ids[$index]);
				}

				// Make sure it's not an empty string
				if (empty($form_id))
				{
					unset($subscriber_form_ids[$index]);
				}
			}
		}

		return $subscriber_form_ids;
	}
}

// EOF
