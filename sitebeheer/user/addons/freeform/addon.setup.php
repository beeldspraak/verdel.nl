<?php
/**
 * Freeform Classic for ExpressionEngine
 *
 * @package       Solspace:Freeform
 * @author        Solspace, Inc.
 * @copyright     Copyright (c) 2008-2021, Solspace, Inc.
 * @link          https://docs.solspace.com/expressionengine/freeform/v2/
 * @license       https://docs.solspace.com/license-agreement/
 */

$info	= array(
	'author'			=> 'Solspace',
	'author_url'		=> 'https://docs.solspace.com/',
	'docs_url'			=> 'https://docs.solspace.com/expressionengine/freeform/v2/',
	'name'				=> 'Freeform Classic',
	'description'		=> 'The original form building add-on for ExpressionEngine.',
	'version'			=> '6.0.5',
	'namespace'			=> 'Solspace\Addons\Freeform',
	'settings_exist'	=> true,
	'freeform_pro'		=> true,
	'models'			=> array(),
	//non-EE info
	'doc_links'		=> array(
		'custom_fields'		=> 'https://docs.solspace.com/expressionengine/freeform/v2/'
	),
);

return $info;
