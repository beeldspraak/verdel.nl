<div class="box panel">
    <div class="panel-heading">
        <h3 class="title-bar__title">Update Indexes</h3>
    </div>

    <div class="panel-body">
<?php
$current_type = '';
foreach ($source_list as $data) {
    echo '<div class="assets-sync-item sync_' . $data->type . '_' . $data->id . '" ><label for="' . $data->type . '_' . $data->id . '">' . $data->name . (isset($data->site_id) && $data->site_id != 1 ? ' [' . $data->site_id . ']' : '') . '</label> <input type="checkbox" class="indexing" id="' . $data->type . '_' . $data->id . '" /></div>';
}
?>
        <br />
    </div>

    <div class="panel-footer">
        <div class="form-btns">
            <input type="submit" class="button button--primary assets-index" value="<?php echo lang('update_indexes') ?>" />
        </div>

        <div id="assets-dialog" class="alert">
            <div class="alert__icon">
                <i class="fas fa-fw"></i>
            </div>
            <div id="index-message" class="alert__content"></div>
            <div id="index-status-report"></div>
        </div>
    </div>
</div>