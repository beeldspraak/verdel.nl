<?php

namespace WesBaker\Subscriber\Model;

use ExpressionEngine\Service\Model\Model;

class SubscriberForm extends Model {

	protected static $_primary_key = 'id';
	protected static $_table_name = 'subscriber_forms';

	protected $id;
	protected $form_name;
	protected $provider;
	protected $method;
	protected $email_field;
	protected $switch_field;
	protected $switch_value;
	protected $unsubscribe;
	protected $settings;

	protected static $_typed_columns = array(
		'id'          => 'int',
		'unsubscribe' => 'yesNo',
		'settings'    => 'json'
	);

	protected static $_validation_rules = array(
		'form_name' => 'required',
		'provider' => 'required|enum[campaign_monitor,mailchimp]',
		'method' => 'required|enum[everyone,switch]',
		'email_field' => 'required',

		'switch_field' => 'whenPresent[switch_value]|required',
		'switch_value' => 'whenPresent[switch_field]|required',

		'unsubscribe' => 'required|enum[y,n]'
	);
}
