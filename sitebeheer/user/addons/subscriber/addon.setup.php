<?php

return array(
	'name'           => 'Subscriber',
	'version'        => '6.0.0',
	'author'         => 'Wes Baker',
	'author_url'     => 'http://wesbaker.com',
	'description'    => 'Freeform Extension that sends Names, Email Addresses and custom fields to email newsletter providers.',
	'namespace'      => 'WesBaker\Subscriber',
	'settings_exist' => TRUE,
	'models'         => array(
		'SubscriberForm' => 'Model\SubscriberForm'
	)
);
