<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'subscriber/config.php';

/**
* Addon Module Update
*/
class Subscriber_upd
{
	var $version = SUBSCRIBER_VER;

	/**
	 * Installs the module, creating the necessary tables
	 */
	public function install()
	{
		// Insert module information into Modules table
		$data = array(
			"module_name"        => SUBSCRIBER_MACHINE,
			"module_version"     => $this->version,
			"has_cp_backend"     => "y",
			"has_publish_fields" => "n"
		);
		ee()->db->insert('modules', $data);

		// Create new database tables
		ee()->load->dbforge();

		// Forms table
		$forms_table = array(
			'id'           => array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE, 'auto_increment' => TRUE),
			'form_name'    => array('type' => 'varchar', 'constraint' => '128'),
			'provider'     => array('type' => 'varchar', 'constraint' => '40', 'default' => 'campaign_monitor'),
			'method'       => array('type' => 'varchar', 'constraint' => '40'),
			'email_field'  => array('type' => 'varchar', 'constraint' => '128'),
			'switch_field' => array('type' => 'varchar', 'constraint' => '128'),
			'switch_value' => array('type' => 'varchar', 'constraint' => '128'),
			'unsubscribe'  => array('type' => 'char', 'constraint' => '1', 'default' => 'n'),
			'settings'     => array('type' => 'text')
		);
		ee()->dbforge->add_field($forms_table);
		ee()->dbforge->add_key('id', TRUE);
		ee()->dbforge->create_table(SUBSCRIBER_DB_FORMS);

		// Add Hooks
		$this->_register_hooks(array(
			'freeform_module_insert_begin'        => 'insert_subscriber_from_freeform',
			'freeform_next_submission_after_save' => 'insert_subscriber_from_freeform_next',
			'channel_form_submit_entry_end'       => 'insert_subscriber_from_channel_form',
			'email_module_send_email_end'         => 'insert_subscriber_from_email',
			'insert_comment_end'                  => 'insert_subscriber_from_comment',
			'member_member_register'              => 'insert_subscriber_from_member_registration',
			'user_register_end'                   => 'insert_subscriber_from_user',
			'user_edit_end'                       => 'insert_subscriber_from_user',
			'cartthrob_create_member'             => 'insert_subscriber_from_cartthrob',
			'cartthrob_pre_process'               => 'insert_subscriber_from_cartthrob',
			'formgrab_save_submission'			  => 'insert_subscriber_from_formgrab',
		));

		// Add field to Freeform
		$this->_add_field();

		return TRUE;
	}

	/**
	 * Uninstalls the module, removing the database tables
	 */
	public function uninstall()
	{
		$module_id = ee()->db->select('module_id')
			->get_where(
				'modules',
				array('module_name' => SUBSCRIBER_MACHINE)
			)
			->row('module_id');

		ee()->db->where('module_name', SUBSCRIBER_MACHINE)
			->delete('modules');

		ee()->db->where('class', SUBSCRIBER_MACHINE)
			->delete('actions');

		ee()->load->dbforge();
		ee()->dbforge->drop_table(SUBSCRIBER_DB_FORMS);

		ee()->db->where('class', SUBSCRIBER_MACHINE.'_ext')
			->delete('extensions');

		return TRUE;
	}

	/**
	 * Update the module's database tables if necessary
	 * @param String $current The installed module's current version
	 */
	public function update($current = '')
	{
		if (version_compare($current, $this->version, '=='))
		{
			return FALSE;
		}

		ee()->load->dbforge();

		// 3.0.1 added a bug fix where the freeform_entries table didn't
		// have the subscriber_form_id column
		if (version_compare($current, '3.0.1', '<'))
		{
			$this->_add_field();
		}

		// 3.0.2 fixed a bug where the method column wasn't long enough for the
		// values it was supposed to contain. This increases the size of the
		// column and also renames the broken value
		if (version_compare($current, '3.0.2', '<'))
		{
			// First increase the size of the column
			$column_data = array(
				'method' => array(
					'name'       => 'method',
					'type'       => 'varchar',
					'constraint' => 40
				)
			);

			ee()->dbforge->modify_column(SUBSCRIBER_DB_FORMS, $column_data);

			// Then correct switch_fie to switch_field
			ee()->db->update(
				SUBSCRIBER_DB_FORMS,
				array('method' => 'switch_field'),
				array('method' => 'switch_fie')
			);
		}

		// Add provider column to support MailChimp vs Campaign Monitor
		if (version_compare($current, '3.1', '<'))
		{
			$column_data = array(
				'provider' => array(
					'type'       => 'varchar',
					'constraint' => 40,
					'default'    => 'campaign_monitor'
				)
			);

			ee()->dbforge->add_column(SUBSCRIBER_DB_FORMS, $column_data);
		}

		// Add first name and last name fields for Mailchimp
		if (version_compare($current, '3.3', '<'))
		{
			$column_data = array(
				'first_name_field' => array(
					'type'       => 'varchar',
					'constraint' => '128'
				)
			);

			ee()->dbforge->add_column(SUBSCRIBER_DB_FORMS, $column_data, 'name_field');

			$column_data = array(
				'last_name_field' => array(
					'type'       => 'varchar',
					'constraint' => '128'
				)
			);

			ee()->dbforge->add_column(SUBSCRIBER_DB_FORMS, $column_data, 'first_name_field');
		}

		// Change the length of the api_key and list_id columns
		if (version_compare($current, '3.4.2', '<'))
		{
			$column_data = array(
				'api_key' => array(
					'name'       => 'api_key',
					'type'       => 'varchar',
					'constraint' => '50'
				),
				'list_id' => array(
					'name'       => 'list_id',
					'type'       => 'varchar',
					'constraint' => '50'
				)
			);

			ee()->dbforge->modify_column(SUBSCRIBER_DB_FORMS, $column_data);
		}

		if (version_compare($current, '4.0', '<'))
		{
			// Add the settings column
			ee()->dbforge->add_column(
				SUBSCRIBER_DB_FORMS,
				array('settings' => array('type' => 'TEXT'))
			);

			// Convert columns into a settings field which is a JSON encoded array
			$forms = ee()->db->get(SUBSCRIBER_DB_FORMS);
			foreach ($forms->result() as $form)
			{
				if ($form->provider == 'campaign_monitor')
				{
					$settings = array(
						'cm_api_key'       => $form->api_key,
						'cm_list_id'       => $form->list_id,
						'cm_name_field'    => $form->name_field,
						'cm_custom_fields' => unserialize($form->custom_fields)
					);
				}
				else if ($form->provider == 'mailchimp')
				{
					$settings = array(
						'mc_api_key'          => $form->api_key,
						'mc_list_id'          => $form->list_id,
						'mc_first_name_field' => $form->first_name_field,
						'mc_last_name_field'  => $form->last_name_field,
						'mc_custom_fields'    => unserialize($form->custom_fields)
					);
				}

				ee()->db->update(
					SUBSCRIBER_DB_FORMS,
					array('settings' => json_encode($settings)),
					array('id'       => $form->id)
				);
			}

			// Delete the old columns
			$old_fields = array('api_key', 'list_id', 'name_field',
				'first_name_field', 'last_name_field', 'custom_fields');
			foreach ($old_fields as $column)
			{
				ee()->dbforge->drop_column(SUBSCRIBER_DB_FORMS, $column);
			}

			// Register new hooks
			$this->_register_hooks(array(
				'channel_form_submit_entry_end' => 'insert_subscriber_from_channel_form',
				'email_module_send_email_end'   => 'insert_subscriber_from_email'
			));
		}

		// I missed converting the names of the custom fields in 4.0.0, so I'm
		// renaming them here
		if (version_compare($current, '4.0.2', '<'))
		{
			$forms = ee('Model')->get('subscriber:SubscriberForm')->all();

			foreach ($forms as $form)
			{
				$custom_field_name = ($form->provider == 'campaign_monitor')
					? 'cm_custom_fields'
					: 'mc_custom_fields';
				$custom_fields = $form->settings[$custom_field_name];

				// Clean up each custom field
				foreach ($custom_fields as $index => $custom_field)
				{
					// If it's already done, skip it
					if ( ! isset($custom_field['name']) AND ! isset($custom_field['tag']))
					{
						continue;
					}

					$custom_field['field_name'] = $custom_field['name'];
					unset($custom_field['name']);

					if ($row->provider == 'campaign_monitor')
					{
						$custom_field['personalization_tag'] = $custom_field['tag'];
						unset($custom_field['tag']);
					}
					else if ($row->provider == 'mailchimp')
					{
						$custom_field['merge_tag'] = $custom_field['tag'];
						unset($custom_field['tag']);
					}

					$custom_fields[$index] = $custom_field;
				}

				// Replace the custom fields and save
				$form->settings[$custom_field_name] = $custom_fields;
				$form->save();
			}
		}

		// Forgot to change the extension method for the freeform hook
		if (version_compare($current, '4.0.3', '<'))
		{
			ee()->db->update(
				'extensions',
				array('method' => 'insert_subscriber_from_freeform'),
				array(
					'hook'  => 'freeform_module_insert_begin',
					'class' => SUBSCRIBER_NAME.'_ext'
				)
			);
		}

		// Register hook for comment formi
		if (version_compare($current, '4.1.0', '<'))
		{
			$this->_register_hooks(array(
				'insert_comment_end' => 'insert_subscriber_from_comment'
			));
		}

		if (version_compare($current, '4.2.0', '<'))
		{
			$this->_register_hooks(array(
				'member_member_register' => 'insert_subscriber_from_member_registration'
			));
		}

		$this->add_columns_for_model($current);

		// Update custom fields data
		if (version_compare($current, '5.0.0', '<'))
		{
			$forms = ee('Model')->get('subscriber:SubscriberForm')->all();

			foreach ($forms as $form)
			{
				foreach (array('cm_custom_fields', 'mc_custom_fields') as $type)
				{
					$custom_fields = array('rows' => array());
					if (isset($form->settings[$type]))
					{
						$index = 1;
						foreach ($form->settings[$type] as $row)
						{
							// Update 'yes' to 'y'
							if (isset($row['multiple']) && $row['multiple'] == 'yes')
							{
								$row['multiple'] = 'y';
							}

							$custom_fields['rows']['row_id_'.$index] = $row;
							$index++;
						}
					}

					$form->settings[$type] = $custom_fields;
				}

				$form->save();
			}
		}

		// Add missing hooks
		if (version_compare($current, '5.0.1', '<'))
		{
			$this->_register_hooks(array(
				'freeform_module_insert_begin'  => 'insert_subscriber_from_freeform',
				'channel_form_submit_entry_end' => 'insert_subscriber_from_channel_form',
				'email_module_send_email_end'   => 'insert_subscriber_from_email',
				'insert_comment_end'            => 'insert_subscriber_from_comment',
				'member_member_register'        => 'insert_subscriber_from_member_registration'
			));
		}

		if (version_compare($current, '5.1.0', '<'))
		{
			$this->_register_hooks(array(
				'user_register_end' => 'insert_subscriber_from_user'
			));
		}

		if (version_compare($current, '5.1.1', '<'))
		{
			$this->_register_hooks(array(
				'user_edit_end' => 'insert_subscriber_from_user',
			));
		}

		if (version_compare($current, '5.2.0', '<'))
		{
			$this->_register_hooks(array(
				'freeform_next_submission_after_save' => 'insert_subscriber_from_freeform_next'
			));
		}

		if (version_compare($current, '5.3.0', '<'))
		{
			$this->_register_hooks(array(
				'cartthrob_create_member' => 'insert_subscriber_from_cartthrob',
				'cartthrob_on_authorize' => 'insert_subscriber_from_cartthrob',
			));
		}

		if (version_compare($current, '5.3.1', '<'))
		{
			$this->_register_hooks(array(
				'cartthrob_on_processing' => 'insert_subscriber_from_cartthrob',
			));
		}

		if (version_compare($current, '5.3.2', '<'))
		{
			$this->_register_hooks(array(
				'formgrab_save_submission' => 'insert_subscriber_from_formgrab',
			));
		}

		if (version_compare($current, '5.3.3', '<'))
		{
			$this->_register_hooks(array(
				'cartthrob_pre_process' => 'insert_subscriber_from_cartthrob',
			));

			ee()->db->delete('exp_extensions', array('hook' => 'cartthrob_on_authorize'));
			ee()->db->delete('exp_extensions', array('hook' => 'cartthrob_on_processing'));
		}

		return TRUE;
	}

	/**
	 * Add Subscriber Form ID to Freeform 3.x automatically
	 */
	private function _add_field()
	{
		if ( ! ee()->db->table_exists('freeform_fields'))
		{
			return;
		}

		// Which version of Freeform are we dealing with?
		$version = ee()->db->select('module_version')
			->get_where('modules', array('module_name' => 'Freeform'))
			->row('module_version');
		$freeform_4 = (version_compare($version, '4.0', '>=')) ? TRUE : FALSE;

		if ( ! $freeform_4)
		{
			// Add field if necessary
			ee()->db->where('name', 'subscriber_form_id');
			if (ee()->db->count_all_results('freeform_fields') <= 0)
			{
				$field_data = array(
					'field_order'  => '1000',
					'field_type'   => 'text',
					'field_length' => '150',
					'name'         => 'subscriber_form_id',
					'label'        => 'Subscriber Form ID (Do not delete)',
					'editable'     => 'n',
					'status'       => 'open'
				);

				ee()->db->insert('freeform_fields', $field_data);
			}

			// Add column if necessary
			ee()->load->dbforge();

			$column_data = array(
				'subscriber_form_id' => array(
					'type'       => 'varchar',
					'constraint' => 150,
					'default'    => ''
				)
			);

			ee()->dbforge->add_column('freeform_entries', $column_data);
		}
	}

	/**
	 * Register a hook and it's method, ensuring it doesn't already exist
	 *
	 * @param  array  $hooks Associative array containing the method name as the
	 *                       key and the hook as the value
	 * @return void
	 */
	private function _register_hooks(array $hooks)
	{
		foreach ($hooks as $hook => $method)
		{
			// Check to make sure the hook doesn't already exist
			$count = ee()->db->where('class', SUBSCRIBER_NAME.'_ext')
				->where(compact('hook', 'method'))
				->count_all_results('exp_extensions');

			if ($count == 0)
			{
				ee()->db->insert(
					'exp_extensions',
					array(
						'class'    => SUBSCRIBER_NAME.'_ext',
						'method'   => $method,
						'hook'     => $hook,
						'settings' => '',
						'priority' => 10,
						'version'  => $this->version,
						'enabled'  => 'y'
					)
				);
			}

		}
	}

	/**
	 * Add Columns that are needed for the model
	 *
	 * All columns that will be added in subsequent versions should be added
	 * here and not to the main update() function.
	 *
	 * @param String $current The current version of the add-on
	 */
	private function add_columns_for_model($current)
	{
		if (version_compare($current, '5.1.0', '<'))
		{
			ee()->dbforge->add_column(SUBSCRIBER_DB_FORMS, array(
				'unsubscribe' => array(
					'type'       => 'char',
					'constraint' => 1,
					'default'    => 'n'
				)
			));
		}
	}
}

// EOF
