CKEDITOR.stylesSet.add( 'verdel_styles', [
    { name: 'Lead paragraaf', element: 'p', attributes: { 'class': 'lead' } },
    { name: 'Normale paragraaf', element: 'p', attributes: { 'class': '' } },
    { name: 'Lees verder link', element: 'p', attributes: { 'class': 'continue' } },
    { name: 'Quote', element: 'blockqoute', attributes: { 'class': 'blockquote' } }
] );