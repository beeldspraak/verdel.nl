<?php
echo '<select name="s3_bucket">';

foreach ($bucket_list as $bucket_name => $bucket_data) {
    $selected = '';
    if ($bucket_name == $source_settings->bucket) {
        $selected = ' selected="selected"';
    }

    echo '<option value="' . $bucket_name . '" data-location="' . $bucket_data->location . '" data-url-prefix="' . "" . '"' . $selected . '>' .
        $bucket_name .
        '</option>';
}

echo '</select>';
