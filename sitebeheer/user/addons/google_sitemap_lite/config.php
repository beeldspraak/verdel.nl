<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Default config
 *
 * @package		Default module
 * @category	Modules
 * @author		Rein de Vries <info@reinos.nl>
 * @link		http://reinos.nl
 * @copyright 	Copyright (c) 2015 Reinos.nl Internet Media
 */

//contants
if ( ! defined('GSL_NAME'))
{
	define('GSL_NAME', 'Google Sitemap Lite');
	define('GSL_CLASS', 'Google_sitemap_lite');
	define('GSL_MAP', 'google_sitemap_lite');
	define('GSL_VERSION', '2.0.1');
	define('GSL_DESCRIPTION', 'Generate Google Sitemap based on the Structure/Taxonomy/Navee module');
	define('GSL_DOCS', 'http://reinos.nl/add-ons/google-sitemap-lite');
	define('GSL_AUTHOR', 'Rein de Vries');
	define('GSL_AUTHOR_URL', 'http://reinos.nl/add-ons');
	define('GSL_STATS_URL', 'http://reinos.nl/index.php/module_stats_api/v1');
}

//configs
$config['name'] = GSL_NAME;
$config['version'] = GSL_VERSION;

//load compat file
require_once(PATH_THIRD.GSL_MAP.'/compat.php');

/* End of file config.php */
/* Location: /system/expressionengine/third_party/default/config.php */