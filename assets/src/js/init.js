$(document).ready(function(){
	
	new WOW().init();

	$('.section--block-grid .block.match-height').matchHeight({
		property: 'min-height',
		byRow: false,
	});

	$('.block--article-list .block--article .match-height').matchHeight({
		property: 'min-height',
		byRow: false,
	});

	var ictWaarborgCarousel = new Swiper('.section--ict-waarborg .swiper-container--ictw', {
	    speed: 400,
	    spaceBetween: 0,
	    autoPlay: true,
	    loop: true,
	    slidesPerView: 1,
	    initialSlide: 0,
        nextButton: '.swiper-container--ictw .swiper-button-next',
        prevButton: '.swiper-container--ictw .swiper-button-prev'
	});

	var projectCarousel = new Swiper('.section--projects .swiper-container--projects', {
		spaceBetween: 6,
		slidesPerView: 4,
		nextButton: '.swiper-container--projects .swiper-button-next',
		prevButton: '.swiper-container--projects .swiper-button-prev',
		breakpoints: {
			480: {
				slidesPerView: 1,
				spaceBetween: 6
			},
			768: {
				slidesPerView: 2,
				spaceBetween: 6
			},
			992: {
				slidesPerView: 3,
				spaceBetween: 6
			}
		}
	});

	var teamCarrousel = new Swiper('.section--team-carrousel .swiper-container', {
		spaceBetween: 0,
		slidesPerView: 'auto',
		nextButton: '.section--team-carrousel .swiper-button-next',
		prevButton: '.section--team-carrousel .swiper-button-prev',
	});

	var mastheadCarousel = new Swiper('.section--masthead .swiper-container', {
		autoPlay: true,
        nextButton: '.section--masthead__swiper .swiper-button-next',
        prevButton: '.section--masthead__swiper .swiper-button-prev',
	    spaceBetween: 10,
	});

	// override jquery validate plugin defaults
	$.validator.setDefaults({
	    highlight: function(element) {
	        $(element).closest('.form-group').addClass('has-error');
	    },
	    unhighlight: function(element) {
	        $(element).closest('.form-group').removeClass('has-error');
	    },
	    errorElement: 'span',
	    errorClass: 'help-block',
	    errorPlacement: function(error, element) {
	        if(element.parent('.input-group').length) {
	            error.insertAfter(element.parent());
	        } 
			else if(element.attr('name') == 'appreciation') {
				error.insertAfter('.appreciation-validation-messages');
			} else {
	            error.insertAfter(element);
	        }
	    }
	});

	$('.form.validate').each(function() { 
		$(this).validate();
	});

	$('a[href*="#"]').smoothScroll({
		offset: -180,
		speed: 350
	});
	
});
		        
var verdel_styles = [{
        featureType: "water", 
        elementType: "all",
        stylers: []
    },{
        featureType: "all", 
        elementType: "geometry",
        stylers: [
            { saturation: -100 }, 
            { visibility: "simplified" }
        ]
    },{ 
        featureType: "all", 
        elementType: "labels",
        stylers: [
            { saturation: -100 }, 
            { lightness: 12 },
            { invert_lightness: true },
            { visibility: "off" }
        ]
    },{ 
        featureType: "road.local", 
        elementType: "labels",
        stylers: [
            { visibility: "simplified" }]
    },{ 
        featureType: "administrative.locality", 
        elementType: "labels", 
        stylers: [
            { visibility: "on" },
            { invert_lightness: true },
            { gamma: 0.22 }]
    }
];
