<?php

require_once PATH_THIRD.'/subscriber/providers/provider.php';
require_once PATH_THIRD.'/subscriber/vendor/autoload.php';

use \DrewM\MailChimp\MailChimp;

class Provider_mailchimp extends Provider
{
	public $name   = 'MailChimp';
	public $prefix = 'mc';
	protected $custom_fields = array(
		'field_name' => 'input',
		'merge_tag'  => 'input'
	);

	/**
	 * Sends the new subscriber to the provider
	 * @param  Array $data     Form data provided by Freeform
	 * @param  Model $model The SubscriberForm model
	 * @return Void
	 */
	public function subscribe($data, $model)
	{
		$mailchimp = new MailChimp($model->settings['mc_api_key']);
		$list_id = $this->getListId($data, $model);

		// Create the name data
		$subscriber_data = array();
		foreach (array('LNAME' => 'mc_last_name_field', 'FNAME' => 'mc_first_name_field') as $key => $field_name)
		{
			if (isset($model->settings[$field_name])
				&& isset($data[$model->settings[$field_name]]))
			{
				$subscriber_data[$key] = $data[$model->settings[$field_name]];
			}
		}

		if (isset($model->settings['mc_custom_fields']))
		{
			foreach ($model->settings['mc_custom_fields']['rows'] as $row_id => $field_settings)
			{
				$field_data = (isset($data[$field_settings['field_name']]))
					? $data[$field_settings['field_name']]
					: '';

				// Merge multiple selected items
				if (is_array($field_data))
				{
					$field_data = implode(',', $field_data);
				}

				$subscriber_data[$field_settings['merge_tag']] = $field_data;
			}
		}

		// Check for single opt-in
		$double_optin = isset($model->settings['mc_options']['single_optin'])
			&& $model->settings['mc_options']['single_optin'] !== 'y';

		$email_address = $data[$model->email_field];
		$subscriber_hash = $mailchimp->subscriberHash($email_address);
		$result = $mailchimp->put("lists/{$list_id}/members/{$subscriber_hash}", [
			'email_address' => $email_address,
			'status'        => ($double_optin) ? 'pending' : 'subscribed',
			'merge_fields'  => $subscriber_data,
			'email_type'    => 'html'
		]);

		if ( ! $mailchimp->success()) {
			$this->_log($result['status'], $result['title'].': '.$result['detail']);
		}
	}

	/**
	 * Unsubscribe a user from the provider
	 *
	 * @param  Array $data     Form data provided by Freeform
	 * @param  Model $model The SubscriberForm model
	 * @return void
	 */
	public function unsubscribe($data, $model)
	{
		$mailchimp = new MailChimp($model->settings['mc_api_key']);
		$list_id = $this->getListId($data, $model);

		$subscriber_hash = $mailchimp->subscriberHash($data[$model->email_field]);
		$result = $mailchimp->delete("lists/{$list_id}/members/{$subscriber_hash}");

		if ( ! $mailchimp->success()) {
			$this->_log($result['status'], $result['title'].': '.$result['detail']);
		}
	}

	/**
	 * Returns an associative array containing data about the API key and list
	 * ID form fields
	 * @param Model $model The SubscriberForm model
	 * @return Array Associative array
	 */
	public function provider_fields($model)
	{
		return array(
			array(
				'title' => 'api_key',
				'desc' => 'mc_api_key_desc',
				'group' => 'mailchimp',
				'fields' => array(
					'settings[mc_api_key]' => array(
						'type' => 'text',
						'required' => TRUE,
						'value' => getValue($model, 'settings.mc_api_key')
					)
				)
			),
			array(
				'title' => 'audience_id',
				'desc' => 'mc_list_id_desc',
				'group' => 'mailchimp',
				'fields' => array(
					'settings[mc_list_id]' => array(
						'type' => 'text',
						'required' => TRUE,
						'value' => getValue($model, 'settings.mc_list_id')
					)
				)
			),
			array(
				'title' => 'single_optin',
				'desc' => 'mc_option_single_optin_desc',
				'group' => 'mailchimp',
				'fields' => array(
					'settings[mc_options][single_optin]' => array(
						'type' => 'yes_no',
						'value' => getValue($model, 'settings.mc_options.single_optin', 'n')
					)
				)
			)
		);
	}

	/**
	 * Returns an associative array containing data about the name fields
	 * @param Model $model The SubscriberForm model
	 * @return Array Associative array
	 */
	public function method_fields($model)
	{
		return array(
			array(
				'title' => 'mc_first_name_field',
				'desc' => 'name_note',
				'group' => 'mailchimp',
				'fields' => array(
					'settings[mc_first_name_field]' => array(
						'type' => 'text',
						'value' => getValue($model, 'settings.mc_first_name_field', 'first_name')
					)
				)
			),
			array(
				'title' => 'mc_last_name_field',
				'group' => 'mailchimp',
				'fields' => array(
					'settings[mc_last_name_field]' => array(
						'type' => 'text',
						'value' => getValue($model, 'settings.mc_last_name_field', 'last_name')
					)
				)
			),
		);
	}
}

// EOF
