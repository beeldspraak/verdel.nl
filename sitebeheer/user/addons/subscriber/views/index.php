<div class="box">
	<?=form_open($base_url, 'class="tbl-ctrls"')?>
		<fieldset class="tbl-search right">
			<a href="<?=$new_form?>" class="btn tn action">
				<?=lang('new_form')?>
			</a>
		</fieldset>
		<form>
			<h1><?= lang('subscriber_forms') ?></h1>
			<?php $this->embed('ee:_shared/table', $table); ?>
			<?php if ( ! empty($table['columns']) && ! empty($table['data'])): ?>
			<fieldset class="tbl-bulk-act hidden">
				<select name="bulk_action">
					<option value="">-- <?=lang('with_selected')?> --</option>
					<option value="remove" data-confirm-trigger="selected" rel="modal-confirm-remove"><?=lang('remove')?></option>
				</select>
				<input class="btn submit" data-conditional-modal="confirm-trigger" type="submit" value="<?=lang('submit')?>">
			</fieldset>
			<?php endif; ?>
		</form>
	</div>
	<?=form_close();?>
</div>

<?php

$modal_vars = array(
	'name'     => 'modal-confirm-remove',
	'form_url' => ee('CP/URL')->make('addons/settings/subscriber/delete')
);
$modal_html = ee('View')->make('ee:_shared/modal_confirm_remove')->render($modal_vars);
ee('CP/Modal')->addModal('remove', $modal_html);
