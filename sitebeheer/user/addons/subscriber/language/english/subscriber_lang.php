<?php

require_once PATH_THIRD.'subscriber/config.php';

$lang = array(

	'duplicate'                     => 'Duplicate',

	// Module Information
	'subscriber_module_name'        => SUBSCRIBER_NAME,
	'subscriber_module_description' => SUBSCRIBER_DESC,
	'subscriber_forms'              => SUBSCRIBER_NAME.' Forms',
	'subscriber_form'               => SUBSCRIBER_NAME.' Form',

	// Navigation
	'new_form'  => 'Create New Form',
	'dashboard' => 'Dashboard',

	// Generic
	'everyone'                 => 'Everyone',
	'switch_field'             => 'Switch Field',
	'form_name'                => 'Form Name',
	'form_names'               => 'Form Names',
	'form_not_created'         => 'Form not created',
	'token'                    => 'Token',
	'no_forms'                 => 'No Forms',
	'provider'                 => "Provider",
	'newsletter_provider'      => "Newsletter Provider",

	// Method
	'method'                   => 'Method',
	'add_method'               => 'Add Method',
	'method_desc'              => "You can either add everyone or add based upon a specific field and it's value.",
	'switch_input_field'       => 'Switch Input Field Name',
	'switch_input_field_desc'  => 'This is the value that will determine whether to add the email address to the subscriber list.',
	'switch_input_field_value' => 'Switch Input Field Value',
	'email_field'              => 'Email Input Field Name',
	'name_note'                => 'Comment forms: use <kbd>name</kbd><br />Registration forms: use <kbd>screen_name</kbd> or <kbd>username</kbd>',
	'email_note'               => 'Comment and Registration forms: use <kbd>email</kbd>',
	'unsubscribe'              => 'Unsubscribe?',
	'unsubscribe_desc'         => 'Unsubscribes the email address if the switch field does not match the correct value.',

	// Provider
	'api_key'                  => 'API Key',
	'list_id'                  => 'List ID',
	'audience_id'              => 'Audience ID',
	'single_optin'             => 'Single Opt-in?',

	// Custom Fields
	'custom_fields'    => 'Custom Fields',
	'add_custom_field' => 'Add Custom Field',
	'no_custom_fields' => 'No Custom Fields',
	'field_name'       => 'Field Name',
	'multiple'         => "Multiple Options?",

	// Campaign Monitor Specific
	'campaign_monitor'            => 'Campaign Monitor',
	'cm_name_field'               => 'Name Input Field Name',
	"cm_api_key_desc"             => "Take a look at <a href='https://www.campaignmonitor.com/api/getting-started/#authenticating-api-key'>Campaign Monitor's Documentation</a>.",
	"cm_list_id_desc"             => "Take a look at <a href='https://www.campaignmonitor.com/api/getting-started/#your-list-id'>Campaign Monitor's Documentation</a>.",
	'cm_custom_fields'            => 'You can find the tag name in &ldquo;Your existing field&rdquo; under Personalization under Custom Fields in Campaign Monitor. Also, if you&rsquo;re using a custom field with multiple options, make sure to check the &ldquo;Multiple Options&rdquo; checkbox; and make sure that in <strong>only</strong> the template the input name is an array with square brackets (e.g. color[]).',
	'personalization_tag'         => 'Personalization Tag',
	'cm_option_single_optin_desc' => 'Normally users are added and have to confirm via email, if you want to add them immediately, check this option.',

	// MailChimp
	'mailchimp'                   => 'MailChimp',
	'mc_first_name_field'         => 'First Name Input Field Name',
	'mc_last_name_field'          => 'Last Name Input Field Name',
	"mc_api_key_desc"             => "Take a look at <a href='http://kb.mailchimp.com/article/where-can-i-find-my-api-key/'>MailChimp's Documentation</a>.",
	"mc_list_id_desc"             => "Take a look at <a href='http://kb.mailchimp.com/article/how-can-i-find-my-list-id/'>MailChimp's Documentation</a>.",
	'mc_custom_fields'            => 'You can find the MERGE Tags in the list&rsquo;s Setting&rsquo;s page under List Fields and *|MERGE|* Tags.',
	'merge_tag'                   => '<kbd>*|MERGE|*</kbd> Tag',
	'mc_option_single_optin_desc' => 'Normally users are added and have to confirm via email, if you want to add them immediately, check this option.',

	// Delete
	'form_delete'                   => 'Delete Form',
	'form_delete_left'              => 'Delete &lsquo;',
	'form_delete_right'             => '&rsquo?',
	'form_delete_confirm'           => 'Are you sure you want to delete this form?',

	// Errors
	'switch_field_missing' =>
	"If you want to use the switch field method, you'll need to enter the switch field's name and what the value needs to be to add someone to your list.",
	'api_key_missing' =>
	"Your API Key is missing, please add it. If you don't know where to find it, please look at <a href='http://www.campaignmonitor.com/api/getting-started/'>Campaign Monitor's Documentation</a> or <a href='http://kb.mailchimp.com/article/where-can-i-find-my-api-key/'>MailChimp's Documentation</a>.",
	'list_id_missing' =>
	"Your List ID is missing, please add it. If you don't know where to find it, please look at <a href='http://www.campaignmonitor.com/api/getting-started/'>Campaign Monitor's Documentation</a> or <a href='http://kb.mailchimp.com/article/how-can-i-find-my-list-id/'>MailChimp's Documentation</a>.",
	'install_freeform' =>
	'Please install <a href="http://www.solspace.com/software/detail/freeform/">Solspace\'s Freeform</a> module before installing Subscriber.'
);

// EOF
