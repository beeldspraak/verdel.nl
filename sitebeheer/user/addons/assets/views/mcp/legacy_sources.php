<?php

echo '<h1>' . lang('external_sources') . '</h1>';
echo form_open($edit_source_action);
$this->load->view('mcp/components/source_type_list', array('sources' => $sources));

echo form_submit(array('name' => 'submit', 'value' => lang('add_new_source'), 'class' => 'btn'));
echo form_close();

if (!empty($sources)) {
    echo form_open($delete_source_action, 'id="delete_source"');
    echo '<input id="source_id" type="hidden" name="source_id" value="0" />';
    echo form_close();
}
