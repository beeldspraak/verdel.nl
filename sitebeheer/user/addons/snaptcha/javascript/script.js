$(document).ready(function()
{
    $('select[name=member_registration_validation], select[name=logging]').change(function() {
        if ($(this).val() == 1) {
            $(this).next().show();
        }
        else {
            $(this).next().hide();
        }
    });

    $('select[name=member_registration_validation]').change();
    $('select[name=logging]').change();
});
