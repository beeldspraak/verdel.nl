<?php

require_once 'autoload.php';
$addonJson = json_decode(file_get_contents(__DIR__ . '/addon.json'));

if (! defined('ASSETS_NAME')) {
    define('ASSETS_NAME', $addonJson->name);
    define('ASSETS_VER', $addonJson->version);
    define('ASSETS_DESC', $addonJson->description);
    define('ASSETS_DOCS', 'https://eeharbor.com/assets/documentation');
}

// NSM Addon Updater
$config['name'] = ASSETS_NAME;
$config['version'] = ASSETS_VER;
$config['nsm_addon_updater']['versions_xml'] = 'https://eeharbor.com/assets/releasenotes.rss';

return array(
    'name'              => $addonJson->name,
    'description'       => $addonJson->description,
    'version'           => $addonJson->version,
    'namespace'         => $addonJson->namespace,
    'author'            => 'EEHarbor',
    'author_url'        => 'https://eeharbor.com/assets',
    'docs_url'          => 'https://eeharbor.com/assets/documentation',
    'settings_exist'    => true,
    'fieldtypes'        => array(
        'assets' => array(
            'name' => 'Assets',
            'compatibility' => 'file'
        )
    )
);
