<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Google Sitemap Lite Structure file
 *
 * @package		Google Sitemap Lite
 * @category	Modules
 * @author		Rein de Vries <info@reinos.nl>
 * @link        http://reinos.nl/add-ons/google-sitemap-lite
 * @copyright 	Copyright (c) 2013 Reinos.nl Internet Media
 */

require_once(PATH_THIRD.'google_sitemap_lite/libraries/google_sitemap_lite_base.php');

class Google_sitemap_lite_structure extends Google_sitemap_lite_base
{
    public $options = array();
    public $site_url;
    public $site_map = array();

    /**
     * Set the options
     *
     * @param array $options
     */
    public function set_options($options = array())
    {
        $this->options = $options;
    }

    //--------------------------------------------------------------

    /**
     * Set the site url
     */
    public function set_site_url()
    {
        $site_url = ee()->config->item('site_url');

        // Get the setting from strucure add_trailing_slash
        // http://devot-ee.com/add-ons/support/google-sitemap-lite/viewthread/8703
        $q = ee()->db->select('var_value')
            ->from('structure_settings')
            ->where('var', 'add_trailing_slash')
            ->get();
        if($q->num_rows() > 0)
        {
            $result = $q->row();
            $add_trailing_slash = $result->var_value;
            if($add_trailing_slash == 'n') {
                $site_url = rtrim($site_url, '/');
            }
        }

        $this->site_url = ee()->google_sitemap_lite_lib->loc_escapes($site_url);
    }

    //--------------------------------------------------------------

    /**
     * Build the sitemap
     *
     * @return array
     */
    public function build_sitemap()
    {
        $pages = ee()->extensions->call('wygwam_config', '', '');

        //only if the link types are set
        if(!empty($pages['link_types']))
        {
            //fetch the mode
            $mode = ee()->TMPL->fetch_param('mode') != ''? strtolower(ee()->TMPL->fetch_param('mode')) : '';

            //show only the pages, no listings
            if($mode == 'pages')
            {
                $this->struc_sitemap($pages['link_types']['Structure Pages'], 'Structure Pages');
            }
            //show all
            else
            {
                foreach($pages['link_types'] as $label => $data)
                {
                    $this->struc_sitemap($data, $label);
                }
            }
        }
        return $this->site_map;
    }

    //--------------------------------------------------------------

    /**
     * Structure the data
     *
     * @param $data
     * @param $label
     * @return unknown_type
     */
    private function struc_sitemap($data, $label)
    {
        //settings
        $use_hide_from_nav = ee()->TMPL->fetch_param('use_hide_from_nav') == "yes" ? true : false;

        // Listing
        if (preg_match("/Listing/i", $label)) {
            $listing = true;
        } else {
            $listing = false;
        }

        //get the links from the pages var
        $site_pages = ee()->config->item('site_pages');

        //get the database result for hide_from_nav purpose
        $hide_from_nav = array();
        if($use_hide_from_nav)
        {
            $query = ee()->db->get('structure');
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                {
                    if($row->hidden == 'y')
                    {
                        $hide_from_nav[] = $row->entry_id;
                    }
                }
            }
        }

        //are there any site pages for this site?
        if(isset($site_pages[$this->options['site_id']]['uris']))
        {
            $page_uris = $site_pages[$this->options['site_id']]['uris'];

            // Get the setting from strucure add_trailing_slash
            // http://devot-ee.com/add-ons/support/google-sitemap-lite/viewthread/8703
            $q = ee()->db->select('var_value')
                ->from('structure_settings')
                ->where('var', 'add_trailing_slash')
                ->get();
            if($q->num_rows() > 0)
            {
                $result = $q->row();
                $add_trailing_slash = $result->var_value;
            }
            else
            {
                $add_trailing_slash = 'n';
            }

            //
            if(!empty($data))
            {
                foreach($data as $val)
                {
                    //Get page and listing Entry ids
                    $uri_segment = str_replace(array($this->site_url, ee()->config->item('site_index')), '', $val['url']);
                    //is there a need for a extra segment?
                    if(!$entry_id = array_search($uri_segment, $page_uris))
                    {
                        $entry_id = array_search('/'.$uri_segment, $page_uris);

                        //fix for finding a match http://devot-ee.com/add-ons/support/google-sitemap-lite/viewthread/7539#27765
                        if($entry_id == '')
                        {
                            $entry_id = array_search($uri_segment.'/', $page_uris);
                        }

                        if($entry_id == '')
                        {
                            $entry_id = array_search('/'.$uri_segment.'/', $page_uris);
                        }
                    }

                    //check on the hidden pages
                    if(($use_hide_from_nav && in_array($entry_id, $hide_from_nav)) || (in_array($entry_id, explode('|', $this->options['exclude']))))
                    {
                        continue;
                    }

                    //fetch segments
                    //$segments = ee()->google_sitemap_lite_lib->clean_up_array(explode('/', $val['url']));

                    //get the last modified data
                    $date = ee()->db->get_where('channel_titles', array('entry_id' => $entry_id))->row();

                    if(!empty($date->edit_date))
                    {
                        $date = date('Y-m-d',ee()->localize->format_date('DATE_ATOM', $date->edit_date));
                    }
                    else
                    {
                        $date = date('Y-m-d');
                    }

                    //format array
                    $this->site_map[] = array(
                        'loc' => ee()->google_sitemap_lite_lib->loc_escapes($val['url']).($add_trailing_slash == 'y' ? '/' : ''),
                        'lastmod' => $date,
                        'changefreq' => $listing ? $this->options['changefreq_listing'] : $this->options['changefreq'],
                        'priority' => $listing ? $this->options['prio_listing'] : $this->options['prio']
                    );
                }
            }
        }
    }

}
