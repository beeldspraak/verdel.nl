<?php

require_once PATH_THIRD.'/subscriber/providers/provider.php';
require_once PATH_THIRD.'/subscriber/vendor/autoload.php';

class Provider_campaign_monitor extends Provider
{
	public $name   = 'Campaign Monitor';
	public $prefix = 'cm';
	protected $custom_fields = array(
		'field_name'          => 'input',
		'personalization_tag' => 'input',
		'multiple'            => 'checkbox'
	);

	/**
	 * Sends the new subscriber to the provider
	 * @param  Array $data Form data provided by Freeform
	 * @param  Model $model The SubscriberForm model
	 * @return Void
	 */
	public function subscribe($data, $model)
	{
		$campaign_monitor = new CS_REST_Subscribers(
			$this->getListId($data, $model),
			$model->settings['cm_api_key']
		);

		// Generate custom field data
		$custom_field_data = array();
		if (isset($model->settings['cm_custom_fields']))
		{
			foreach ($model->settings['cm_custom_fields']['rows'] as $row_id => $field_settings)
			{
				if (empty($field_settings['field_name']) OR empty($field_settings['personalization_tag']))
				{
					continue;
				}

				$field_data = (isset($data[$field_settings['field_name']])) ? $data[$field_settings['field_name']] : '';

				if (isset($field_settings['multiple']) && $field_settings['multiple'] == 'y')
				{
					foreach ($field_data as $value)
					{
						$custom_field_data[] = array(
							'Key'	=> $field_settings['personalization_tag'],
							'Value'	=> $value
						);
					}
				}
				elseif ( ! is_array($field_data))
				{
					$custom_field_data[] = array(
						'Key'	=> $field_settings['personalization_tag'],
						'Value'	=> $field_data
					);
				}
			}
		}

		// Build data array to send to Campaign Monitor
		$email = $data[$model->email_field];
		$data = array(
			'EmailAddress'	=> $email,
			'Name'			=> $this->getName($data, $model, 'cm_name_field'),
			'CustomFields'	=> $custom_field_data,
			'Resubscribe'	=> TRUE
		);

		// Are we dealing with an existing subscriber?
		$existing = $campaign_monitor->get($email);
		$existing = ($existing->http_status_code == 400) ? FALSE : TRUE;

		if ($existing)
		{
			$result = $campaign_monitor->update($email, $data);
		}
		else
		{
			$result = $campaign_monitor->add($data);
		}

		if ( ! $result->was_successful())
		{
			$message = (isset($result->response->Message)) ? $result->response->Message : $result->response['message'];
			$this->_log($result->http_status_code, $message);
		}
	}

	/**
	 * Unsubscribe a user from the provider
	 *
	 * @param  Array $data Form data provided by Freeform
	 * @param  Model $model The SubscriberForm model
	 * @return void
	 */
	public function unsubscribe($data, $model)
	{
		$campaign_monitor = new CS_REST_Subscribers(
			$this->getListId($data, $model),
			$model->settings['cm_api_key']
		);
		$result = $campaign_monitor->unsubscribe($data[$model->email_field]);

		if ( ! $result->was_successful())
		{
			$message = (isset($result->response->Message)) ? $result->response->Message : $result->response['message'];
			$this->_log($result->http_status_code, $message);
		}
	}

	/**
	 * Returns an associative array containing data about the API key and list
	 * ID form fields
	 * @param Model $model The SubscriberForm model
	 * @return Array Associative array
	 */
	public function provider_fields($model)
	{
		return array(
			array(
				'title' => 'api_key',
				'desc' => 'cm_api_key_desc',
				'group' => 'campaign_monitor',
				'fields' => array(
					'settings[cm_api_key]' => array(
						'type' => 'text',
						'required' => TRUE,
						'value' => getValue($model, 'settings.cm_api_key')
					)
				)
			),
			array(
				'title' => 'list_id',
				'desc' => 'cm_list_id_desc',
				'group' => 'campaign_monitor',
				'fields' => array(
					'settings[cm_list_id]' => array(
						'type' => 'text',
						'required' => TRUE,
						'value' => getValue($model, 'settings.cm_list_id')
					)
				)
			),
		);
	}

	/**
	 * Returns an associative array containing data about the name fields
	 * @param Model $model The SubscriberForm model
	 * @return Array Associative array
	 */
	public function method_fields($model)
	{
		return array(
			array(
				'title' => 'cm_name_field',
				'desc' => 'name_note',
				'group' => 'campaign_monitor',
				'fields' => array(
					'settings[cm_name_field]' => array(
						'type' => 'text',
						'value' => getValue($model, 'settings.cm_name_field', 'name')
					)
				)
			),
		);
	}
}

// EOF
