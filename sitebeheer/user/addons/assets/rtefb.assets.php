<?php

namespace EEHarbor\Assets;

use ExpressionEngine\Library\Rte\AbstractRteFilebrowser;

class Assets_rtefb extends AbstractRteFilebrowser
{
    public function addJs($uploadDir)
    {
        if (!class_exists('Assets_helper')) {
            require_once 'helper.php';
        }
        // include sheet resources
        \Assets_helper::include_sheet_resources();

        ee()->javascript->output("window.Rte_browseImages = function(sourceElement, params) {
            Assets.initRte(sourceElement, params, '" . $uploadDir . "', 'image');
        }");
    }

    public function getUploadDestinations()
    {
        $msm = false;
        if (ee('Config')->getFile()->getBoolean('multiple_sites_enabled')) {
            $sites = ee('Model')->get('Site')->all()->pluck('site_id', 'site_label');
        }

        ee()->load->add_package_path(PATH_THIRD . 'assets');
        ee()->load->library('assets_lib');
        $all_sources = ee()->assets_lib->get_all_sources();

        $uploadDirs = [];
        foreach ($all_sources as $source) {
            $uploadDirs[$source->type . ':' . $source->id] = ($msm && $source->type == 'ee' ? $sites[$source->site_id] . ' - ' : '') . $source->name;
        }
        return $uploadDirs;
    }

    //This is here only for compatibility with migration of Wygwam fields
    public function replaceTags($data)
    {
        if ($data != null) {
            preg_match_all("/\\{assets_(\\d*):((.*)(\\}))/uU", $data, $matches);

            if ($matches && !empty($matches[0])) {
                $assetIds = $matches[1];
                $assetUrls = $matches[3];

                ee()->load->add_package_path(PATH_THIRD . 'assets');
                ee()->load->library('assets_lib');
                $files = ee()->assets_lib->get_file_by_id($assetIds);

                for ($counter = 0; $counter < count($matches[1]); $counter++) {
                    $fileId = $matches[1][$counter];
                    // The file has been deleted or Assets is not installed.
                    if ((isset($files[$fileId]) && $files[$fileId] === false) || !isset($files[$fileId])) {
                        $replace = $matches[3][$counter];
                    } else {
                        $replace = $files[$fileId]->url();
                    }
                    $data = str_replace('{assets_' . $fileId . ':' . $matches[3][$counter] . '}', $replace, $data);
                }
            }
        }
        return $data;
    }
}
