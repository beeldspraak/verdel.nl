<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '5e6687f3d7c6255a4c7b38052f9487d87c78d421',
        'name' => 'solspace/ee-freeform-next',
        'dev' => false,
    ),
    'versions' => array(
        'apache/log4php' => array(
            'pretty_version' => '2.3.0',
            'version' => '2.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../apache/log4php',
            'aliases' => array(),
            'reference' => 'cac428b6f67d2035af39784da1d1a299ef42fcf2',
            'dev_requirement' => false,
        ),
        'danielstjules/stringy' => array(
            'pretty_version' => '2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../danielstjules/stringy',
            'aliases' => array(),
            'reference' => 'edbda419cbe4bcc3cb200b7c9811cb6597bf058b',
            'dev_requirement' => false,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'reference' => '1febd6c3ef84253d7c815bed85fc622ad207a9f8',
            'dev_requirement' => false,
        ),
        'egulias/email-validator' => array(
            'pretty_version' => '2.1.25',
            'version' => '2.1.25.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../egulias/email-validator',
            'aliases' => array(),
            'reference' => '0dbf5d78455d4d6a41d186da50adc1122ec066f4',
            'dev_requirement' => false,
        ),
        'guzzle/batch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/common' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/guzzle' => array(
            'pretty_version' => 'v3.9.3',
            'version' => '3.9.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzle/guzzle',
            'aliases' => array(),
            'reference' => '0645b70d953bc1c067bbc8d5bc53194706b628d9',
            'dev_requirement' => false,
        ),
        'guzzle/http' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/inflection' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/iterator' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/parser' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-async' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-backoff' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-cookie' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-curlauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-error-response' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-history' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-log' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-md5' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-mock' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/plugin-oauth' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/service' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'guzzle/stream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => 'v3.9.3',
            ),
        ),
        'hashids/hashids' => array(
            'pretty_version' => '1.0.6',
            'version' => '1.0.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../hashids/hashids',
            'aliases' => array(),
            'reference' => '6bd7ceffb2ccddef6cd64e88f1717f97d131cc3c',
            'dev_requirement' => false,
        ),
        'paragonie/random_compat' => array(
            'pretty_version' => 'v9.99.100',
            'version' => '9.99.100.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/random_compat',
            'aliases' => array(),
            'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
            'dev_requirement' => false,
        ),
        'pixel418/markdownify' => array(
            'pretty_version' => 'v2.3.1',
            'version' => '2.3.1.0',
            'type' => 'lib',
            'install_path' => __DIR__ . '/../pixel418/markdownify',
            'aliases' => array(),
            'reference' => '891be2176858712c6c4581da410e96fc2019e3f0',
            'dev_requirement' => false,
        ),
        'solspace/ee-freeform-next' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '5e6687f3d7c6255a4c7b38052f9487d87c78d421',
            'dev_requirement' => false,
        ),
        'symfony/event-dispatcher' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/event-dispatcher',
            'aliases' => array(),
            'reference' => 'a77e974a5fecb4398833b0709210e3d5e334ffb0',
            'dev_requirement' => false,
        ),
        'symfony/filesystem' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/filesystem',
            'aliases' => array(),
            'reference' => '7ae46872dad09dffb7fe1e93a0937097339d0080',
            'dev_requirement' => false,
        ),
        'symfony/finder' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/finder',
            'aliases' => array(),
            'reference' => '1444eac52273e345d9b95129bf914639305a9ba4',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => 'aed596913b70fae57be53d86faa2e9ef85a2297b',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '4ad5115c0f5d5172a9fe8147675ec6de266d8826',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8db0ae7936b42feb370840cf24de1a144fb0ef27',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => 'b5f7b932ee6fa802fc792eabd77c4c88084517ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php70' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php70',
            'aliases' => array(),
            'reference' => '3fe414077251a81a1b15b1c709faf5c2fbae3d4e',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.19.0',
            'version' => '1.19.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => 'beecef6b463b06954638f02378f52496cb84bacc',
            'dev_requirement' => false,
        ),
        'symfony/property-access' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/property-access',
            'aliases' => array(),
            'reference' => 'c8f10191183be9bb0d5a1b8364d3891f1bde07b6',
            'dev_requirement' => false,
        ),
    ),
);
